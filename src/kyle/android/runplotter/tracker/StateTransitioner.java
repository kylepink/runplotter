package kyle.android.runplotter.tracker;

/**
 * Simple interface which defines simple methods for the transition logic of
 * classes which perform Tracking may need to perform.
 * 
 * @author Kyle Pink
 */
public interface StateTransitioner {
	
	/**
	 * Instructs transition change to occur from before session state to running state. 
	 */
	public void performTransStart();
	
	/**
	 * Instructs transition change to occur from running state to paused state. 
	 */
	public void performTransPause();
	
	/**
	 * Instructs transition change to occur from paused state to running state. 
	 */
	public void performTransResume();
	
	/**
	 * Instructs transition change to occur from running state to after state. 
	 */
	public void performTransRunningStop();
	
	/**
	 * Instructs transition change to occur from paused state to after state. 
	 */
	public void performTransPausedStop();
}
