package kyle.android.runplotter.tracker.responders;

import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.tracker.MasterTracker;
import kyle.android.runplotter.tracker.Responder;
import kyle.android.runplotter.tracker.SessionTrackerService;
import kyle.android.runplotter.util.CallStateListenerEvents;
import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Responds to specific telephony call events and performs actions
 * which have been specified within the 
 * 
 * @author Kyle Pink
 */
public class CallEventResponder extends CallStateListenerEvents implements Responder {

	private MasterTracker master;

	//Flags for session state and if currently paused.
	private boolean runningSessionState;
	private boolean actionCurrentlyInitiated;

	//behavioural settings
	private final boolean pauseOnRing; //true == ring, false == answer.
	private final boolean resumeAfterCall;
	private final boolean sameForOutgoingCalls;

	public CallEventResponder(MasterTracker master) {
		this.master = master;

		this.runningSessionState = false;
		this.actionCurrentlyInitiated = false;

		//Retrieves settings.
		RunPlotterSettings settings = master.getApp().getSettings();
		this.pauseOnRing = settings.getPauseOnCallWhen() == RunPlotterSettings.PAUSE_SESSION_ON_CALL_RING; //true == Pause on ring, false == Pause on answer.
		this.resumeAfterCall = settings.isResumeAfterCall();
		this.sameForOutgoingCalls = settings.isSameForOutgoingCalls();
	}

	@Override
	public void onCreate() {
		Context context = master.getAppContext();
		TelephonyManager teleProvider = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		teleProvider.listen(this, PhoneStateListener.LISTEN_CALL_STATE);
	}

	@Override
	public void onDestroy() {
		Context context = master.getAppContext();
		TelephonyManager teleProvider = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		teleProvider.listen(this, PhoneStateListener.LISTEN_NONE);
	}


	//
	//  Tracker Transitions
	//

	@Override
	public void performTransStart() {
		runningSessionState = true;
	}

	@Override
	public void performTransPause() {
		runningSessionState = false;
	}

	@Override
	public void performTransResume() {
		runningSessionState = true;
	}

	@Override
	public void performTransRunningStop() {
		runningSessionState = false;
	}

	@Override
	public void performTransPausedStop() {
		runningSessionState = false;
	}
	

	//
	//  Call state listener method(s).
	//

	//XXX remove method, DEBUGGING PURPOSES ONLY.
	@Override
	public void onCallStateChanged(int state, String incomingNumber) {
		int prevStatus = master.getTrackerStatus();
		
		super.onCallStateChanged(state, incomingNumber);

		Log.i(this.getClass().getName(), "New state: " + pss(state));
		Log.i(this.getClass().getName(), "Tracker - Prev state: " + tss(prevStatus) + ", New state: " + tss(master.getTrackerStatus()));
	}

	@Override
	protected void onCallStateEvent(int event, String number) {
		
		//No action if not in tracking state and no action currently initiated.
		if (!runningSessionState && !actionCurrentlyInitiated) {
			return;
		}
		
		switch (event) {

		case CALL_EVENT_INCOMING_RING:
			if (runningSessionState && pauseOnRing) {
				//Pause
				master.setStatus(SessionTrackerService.STATUS_PAUSED);
				actionCurrentlyInitiated = true;
			}
			break;
			
		case CALL_EVENT_INCOMING_UNANSWER_OR_DROP:
			if (pauseOnRing && actionCurrentlyInitiated) {
				//Resume
				master.setStatus(SessionTrackerService.STATUS_RUNNING);
				actionCurrentlyInitiated = false;
			}
			break;
			
		case CALL_EVENT_INCOMING_ANSWER:
			if (!pauseOnRing) {
				master.setStatus(SessionTrackerService.STATUS_PAUSED);
				actionCurrentlyInitiated = true;
			}
			break;
			
		case CALL_EVENT_OUTGOING_START:
			if (runningSessionState && sameForOutgoingCalls) {
				//Pause here
				master.setStatus(SessionTrackerService.STATUS_PAUSED);
				actionCurrentlyInitiated = true;
			}
			break;

		case CALL_EVENT_INCOMING_FINISH:
		case CALL_EVENT_OUTGOING_FINISH:
			if (actionCurrentlyInitiated) {
				if (resumeAfterCall) {
					//Resume here
					master.setStatus(SessionTrackerService.STATUS_RUNNING);
				}
				actionCurrentlyInitiated = false;
			}
			break;
			
			default: //will never occur.
				break;
		}
	}
	
	//XXX REMOVE ALL METHODS BELOW.
	private String pss(int state) {
		switch(state) {
		case TelephonyManager.CALL_STATE_IDLE:
			return"IDLE";
			
		case TelephonyManager.CALL_STATE_OFFHOOK:
			return"OFFHOOK";
			
		case TelephonyManager.CALL_STATE_RINGING:
			return"RINGING";
			
		default:
			return "UNKNOWN";
		}
	}
	
	private String tss(int state) {
		switch(state) {
		case SessionTrackerService.STATUS_BEFORE:
			return"BEFORE";
			
		case SessionTrackerService.STATUS_RUNNING:
			return "RUNNING";
			
		case SessionTrackerService.STATUS_PAUSED:
			return "PAUSED";
			
		case SessionTrackerService.STATUS_AFTER:
			return "AFTER";
			
		default:
			return "UNKNOWN";
		}
	}
}
