package kyle.android.runplotter.tracker.responders;

import kyle.android.runplotter.tracker.MasterTracker;
import kyle.android.runplotter.tracker.Responder;

//XXX REMOVE
public class InactivityResponder implements Responder {
	
	private MasterTracker master;
	
	public InactivityResponder(MasterTracker master) {
		this.master = master;
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
	}

	
	//
	//  Transitions
	//

	@Override
	public void performTransStart() {
		
	}

	@Override
	public void performTransPause() {
		
	}

	@Override
	public void performTransResume() {
		
	}

	@Override
	public void performTransRunningStop() {
		
	}

	@Override
	public void performTransPausedStop() {
		
	}
	
}
