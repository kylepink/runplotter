package kyle.android.runplotter.tracker.responders;

import java.util.Calendar;

import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.tracker.MasterTracker;
import kyle.android.runplotter.tracker.Responder;
import kyle.android.runplotter.tracker.SessionTrackerService;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

//TODO handle pauses appropriately, ie-tracker being paused for 30mins then alert should be restarted with time maxRunTimeHours-30mins (without stopping tracker or notifying).
//TODO On second thought leave as is, change settings to inform user??
public class MaximumTimeResponder extends BroadcastReceiver implements Responder {
	
	private static final String MAX_TIME_INTENT = "kyle.android.runplotter.service.responders.MaxTimeResponder";

	private MasterTracker master;
	
	private int maxRunTimeHrs;
	private boolean notifyInactivityMaxTimeActivation;
	private boolean saveSessionOnInactivityMaxTime;
	
	private PendingIntent pIntent;
	
	
	public MaximumTimeResponder(MasterTracker master) {
		this.master = master;
		
		RunPlotterSettings settings = master.getApp().getSettings();
		this.maxRunTimeHrs = settings.getMaxRunTimeHrs();
		this.notifyInactivityMaxTimeActivation = settings.isNotifyInactivityMaxTimeActivation();
		this.saveSessionOnInactivityMaxTime = settings.isSaveSessionOnInactivityMaxTime();
	}
	
	@Override
	public void onCreate() {
		Context context = master.getAppContext();
		context.registerReceiver(this, new IntentFilter(MAX_TIME_INTENT));
		
		pIntent = PendingIntent.getBroadcast(context, 0, new Intent(MAX_TIME_INTENT), 0);
	}

	@Override
	public void onDestroy() {
		Context context = master.getAppContext();
		context.unregisterReceiver(this);
		
		//attempts to remove alarm regardless if been set.
		AlarmManager alarmProvider = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmProvider.cancel(pIntent);
	}
	

	//
	//  Core functionality
	//
	
	private void setAlarm() {
		Context context = master.getAppContext();
		
		//Alarm time
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, maxRunTimeHrs);
		
		//Sets alarm		
		AlarmManager alarmProvider = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmProvider.set(AlarmManager.RTC, cal.getTimeInMillis(), pIntent);
	}
	
	

	@Override
	public void onReceive(Context context, Intent intent) {
		
		//Notifies service of impending stop due to max time.
		if (notifyInactivityMaxTimeActivation) {
			notifyIntendedActionWithActionedDelayed();
			return;
		}
		sendMaxTimeResponse();
	}
	
	private void notifyIntendedActionWithActionedDelayed() {
		//TODO must do this.
	}
	
	private void sendMaxTimeResponse() {
		//prepares command, either saving or discarding data. //TODO CHANGE COMMAND_STOP TO COMMAND_KILL_SAVE?
		int commandId = (saveSessionOnInactivityMaxTime ? SessionTrackerService.COMMAND_STOP : SessionTrackerService.COMMAND_KILL_DISCARD);
		
		//Prepares the sending of the command.
		Context context = master.getAppContext();
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
		
		//Sends off command.
		Intent commandIntent = new Intent();
		commandIntent.setAction(SessionTrackerService.INTENT_ACTION_COMMAND);
		commandIntent.putExtra(SessionTrackerService.COMMAND_TYPE, commandId);
		broadcastManager.sendBroadcast(commandIntent);
	}

	
	//
	//  Transitions
	//
	
	@Override
	public void performTransStart() {
		setAlarm();
	}

	@Override
	public void performTransPause() {
	}

	@Override
	public void performTransResume() {
	}

	@Override
	public void performTransRunningStop() {
	}

	@Override
	public void performTransPausedStop() {
	}	
}
