package kyle.android.runplotter.tracker;

import kyle.android.runplotter.util.Component;

/**
 * The Responder interface defines an object which responds to events related
 * to the {@link SessionTrackerService} and then performs some related action.
 * 
 * @author Kyle Pink
 */
public interface Responder extends Component, StateTransitioner { }
