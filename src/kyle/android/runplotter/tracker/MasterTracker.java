package kyle.android.runplotter.tracker;

import java.util.ArrayList;

import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.data.model.SessionData;
import kyle.android.runplotter.data.model.SessionType;
import kyle.android.runplotter.data.model.TrackingType;
import kyle.android.runplotter.tracker.responders.CallEventResponder;
import kyle.android.runplotter.tracker.responders.InactivityResponder;
import kyle.android.runplotter.tracker.responders.MaximumTimeResponder;
import kyle.android.runplotter.tracker.trackers.EnergyTracker;
import kyle.android.runplotter.tracker.trackers.LocationTracker;
import kyle.android.runplotter.tracker.trackers.StepTracker;
import kyle.android.runplotter.tracker.trackers.TimeTracker;
import kyle.android.runplotter.util.Component;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Master tracker which co-ordinates all other {@link Tracker} objects for session.
 * 
 * @see Tracker
 * @author Kyle Pink
 */
public class MasterTracker implements Component {
	
	//Validator for tracker status transitions
	private static final String[] VALID_TRANSITIONS = new String[]{
		SessionTrackerService.STATUS_BEFORE + " " + SessionTrackerService.STATUS_RUNNING,
		SessionTrackerService.STATUS_PAUSED + " " + SessionTrackerService.STATUS_RUNNING,
		SessionTrackerService.STATUS_RUNNING + " " + SessionTrackerService.STATUS_PAUSED,
		SessionTrackerService.STATUS_RUNNING + " " + SessionTrackerService.STATUS_AFTER,
		SessionTrackerService.STATUS_PAUSED + " " + SessionTrackerService.STATUS_AFTER
	};
	
	public static boolean isTrackerStatusTransitionValid(int previousStatus, int newStatus) {
		String transitionValue = previousStatus + " " + newStatus;
		
		for (String value : VALID_TRANSITIONS) {
			if (value.equals(transitionValue))
				return true;
		}
		
		return false;
	}
	

	//component helpers
	private RunPlotterApplication appContext;
	private LocalBroadcastManager localBroadcaster;

	
	//Tracking helpers
	private volatile int status = SessionTrackerService.STATUS_BEFORE;
	private TrackingType trackingType;
	private Object trackingTypeData;
	private SessionType sessionType;


	//Trackers/Responders
	private TimeTracker timeTracker;
	private LocationTracker locationTracker;
	private StepTracker stepTracker;
	private EnergyTracker energyTracker;
	private ArrayList<Responder> responders;
	private Tracker[] trackers;

	
	public MasterTracker(Context context, SessionType sessionType, TrackingType trackingType, Object trackingTypeData) {
		this.appContext = (RunPlotterApplication)context.getApplicationContext();
		this.localBroadcaster = LocalBroadcastManager.getInstance(getAppContext());
		this.sessionType = sessionType;
		this.trackingType = trackingType;
		this.trackingTypeData = trackingTypeData;

		setupTrackers();
		setupResponders();
	}

	private void setupTrackers() {
		timeTracker = new TimeTracker(this);
		locationTracker = new LocationTracker(this);
		stepTracker = new StepTracker(this);
		energyTracker = new EnergyTracker(this);
		trackers = new Tracker[]{
				timeTracker,
				locationTracker,
				stepTracker,
				energyTracker
		};
	}

	private void setupResponders() {
		//Retrieves settings
		RunPlotterSettings settings = getApp().getSettings();
		
		//Creates and adds responders based on settings.
		responders = new ArrayList<Responder>();
		
		if (settings.isPauseOnCall())
			responders.add(new CallEventResponder(this));
		if (settings.isInactivityChecker())
			responders.add(new InactivityResponder(this));
		if (settings.isMaxTime())
			responders.add(new MaximumTimeResponder(this));
	}

	@Override
	public void onCreate() {
		for (Tracker tracker : trackers) {
			tracker.onCreate();
		}
		for (Responder responder : responders) {
			responder.onCreate();
		}
	}


	@Override
	public void onDestroy() {
		for (Responder responder : responders) {
			responder.onDestroy();
		}
		for (Tracker tracker : trackers) {
			tracker.onDestroy();
		}
	}


	//
	//  Getters
	//
	
	public RunPlotterApplication getApp() {
		return appContext;
	}

	public Context getAppContext() {
		return appContext;
	}

	public int getTrackerStatus() {
		return status;
	}

	public TrackingType getTrackingType() {
		return trackingType;
	}

	public SessionType getSessionType() {
		return sessionType;
	}

	
	//
	//  Getters of core tracking info.
	//

	public long getDurationMs() {
		return timeTracker.getRuntimeDurationMs();
	}

	
	//
	//  Core functionality
	//

	public void setStatus(int newStatus) throws IllegalStateException {

		//Ensures that status transition is valid.
		if (!isTrackerStatusTransitionValid(status, newStatus)) {
			throw new IllegalStateException(String.format("MasterTracker.setStatus(%d): Incorrect status set, cannot transition from status %d to %d.", newStatus, status, newStatus));
		}

		//Performs transition
		int oldStatus = status;
		status = newStatus;

		switch(status) {

		case SessionTrackerService.STATUS_RUNNING:
			//Start or Resume.
			if (oldStatus == SessionTrackerService.STATUS_BEFORE) {
				//Session is starting, was not started.
				for (Responder responder : responders) {
					responder.performTransStart();
				}
				for (Tracker tracker : trackers) {
					tracker.performTransStart();
				}
			} else {
				//Session is resuming, was paused.
				for (Responder responder : responders) {
					responder.performTransResume();
				}
				for (Tracker tracker : trackers) {
					tracker.performTransResume();
				}
			}

			break;

		case SessionTrackerService.STATUS_PAUSED:
			//Session is pausing, was running.
			for (Responder responder : responders) {
				responder.performTransPause();
			}
			for (Tracker tracker : trackers) {
				tracker.performTransPause();
			}

			break;

		case SessionTrackerService.STATUS_AFTER:
			//Running stop or paused stop.
			if (oldStatus == SessionTrackerService.STATUS_RUNNING) {
				//Session is stopping(ending), was running.
				for (Responder responder : responders) {
					responder.performTransRunningStop();
				}
				for (Tracker tracker : trackers) {
					tracker.performTransRunningStop();
				}
			} else {
				//Session is stopping(ending), was paused.
				for (Responder responder : responders) {
					responder.performTransPausedStop();
				}
				for (Tracker tracker : trackers) {
					tracker.performTransPausedStop();
				}
			}
			break;

		//Should never happen, as not valid transitions.
		default:
			throw new IllegalStateException(String.format("MasterTracker.setStatus(%d): Incorrect status set, cannot transition from status %d to %d. Marked as valid transition.", newStatus, status, newStatus));
		}

		//Broadcasts updated state
		sendBroadcastUpdate();
	}


	//
	//  Update broadcaster and helper.
	//

	public void sendBroadcastUpdate(){
		int gpsStatus = locationTracker.getGpsProviderStatus();
		float totalDistanceMetres = (float) locationTracker.getTotalDistanceMetres();
		float currentSpeedMps = locationTracker.getCurrentSpeedMps();
		long durationTimeMs = timeTracker.getRuntimeDurationMs();

		//Creates intent to broadcast
		Intent intent = new Intent();
		intent.setAction(SessionTrackerService.INTENT_ACTION_UPDATE);

		//Adds update data.
		intent.putExtra(SessionTrackerService.UPDATE_DATA_INFO_STATUS, status);
		intent.putExtra(SessionTrackerService.UPDATE_DATA_INFO_GPS_PROVIDER_STATUS, gpsStatus);
		intent.putExtra(SessionTrackerService.UPDATE_DATA_INFO_TOTAL_DIST_M, totalDistanceMetres);
		intent.putExtra(SessionTrackerService.UPDATE_DATA_INFO_SPEED_MPS, currentSpeedMps);
		intent.putExtra(SessionTrackerService.UPDATE_DATA_INFO_TIME, durationTimeMs);

		//Will have zero if distance is zero.
		if (totalDistanceMetres != 0) {
			intent.putExtra(SessionTrackerService.UPDATE_DATA_INFO_SPEED_AVG_MPS, (float) (totalDistanceMetres/(durationTimeMs/1000.0)));
		}

		localBroadcaster.sendBroadcast(intent);
	}

	public Session getEndRunSession(){
		//Checks if is in correct state, throws error otherwise.
		if (status != SessionTrackerService.STATUS_AFTER){
			throw new IllegalStateException("Tracker.getEndRunInformation() - attempt to endRun during bad service status.");
		}
		long startTimeDate = timeTracker.getStartTimeDate();
		long duration = timeTracker.getRuntimeDurationMs();
		long pauseDuration = timeTracker.getPauseDurationMs();
		double totalDistance = locationTracker.getTotalDistanceMetres();
		long steps = stepTracker.getSteps();
		long energy = energyTracker.getEnergy();
		SessionData sessionData = locationTracker.getSessionData();


		//Creates tracked info to be returned.
		return new Session(
				startTimeDate,
				sessionType,
				trackingType,
				trackingTypeData,
				duration,
				pauseDuration,
				totalDistance,
				steps,
				energy,
				sessionData);
	}
}
