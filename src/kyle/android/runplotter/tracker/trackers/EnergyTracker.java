package kyle.android.runplotter.tracker.trackers;

import kyle.android.runplotter.tracker.MasterTracker;
import kyle.android.runplotter.tracker.Tracker;

/**
 * Tracks Energy usage of the session. Possible?
 * 
 * @author Kyle Pink
 */
public class EnergyTracker implements Tracker {

	private MasterTracker master;
	
	private long energy;
	
	public EnergyTracker(MasterTracker masterTracker) {
		this.master = masterTracker;
	}

	@Override
	public void onCreate() {
		
	}

	@Override
	public void onDestroy() {

	}
	
	
	//
	//  Transitions
	//

	@Override
	public void performTransStart() {
		
	}

	@Override
	public void performTransPause() {
		
	}

	@Override
	public void performTransResume() {
		
	}

	@Override
	public void performTransRunningStop() {
		
	}

	@Override
	public void performTransPausedStop() {
		
	}
	
	//
	//  Getters
	//
	
	public long getEnergy() {
		return energy;
	}
}
