package kyle.android.runplotter.tracker.trackers;

import kyle.android.runplotter.tracker.MasterTracker;
import kyle.android.runplotter.tracker.SessionTrackerService;
import kyle.android.runplotter.tracker.Tracker;


/**
 * A simple time {@link Tracker} which keeps track of start/end times and the duration of the session and paused time taken.
 * 
 * @author Kyle Pink
 */
public class TimeTracker implements Tracker {

	private MasterTracker master;

	//
	//  Time, tracker and status data
	//

	/* Time variables */
	private long START_TIME;
	private long END_TIME;
	private long startTimeAndPauseTime;

	/* Total pause time for the service. */
	private long startOfPauseTime;
	private long totalPauseTime = 0;

	public TimeTracker(MasterTracker masterTracker) {
		this.master = masterTracker;
	}

	@Override
	public void onCreate() {

	}

	@Override
	public void onDestroy() {

	}
	
	
	//
	//  Transitions
	//

	@Override
	public void performTransStart() {
		START_TIME = System.currentTimeMillis();
		startTimeAndPauseTime = START_TIME;
	}

	@Override
	public void performTransPause() {
		startOfPauseTime = System.currentTimeMillis();
	}

	@Override
	public void performTransResume() {
		totalPauseTime += (System.currentTimeMillis() - startOfPauseTime);
		startTimeAndPauseTime = START_TIME + totalPauseTime;
	}

	@Override
	public void performTransRunningStop() {
		END_TIME = System.currentTimeMillis();
	}

	@Override
	public void performTransPausedStop() {
		long currentTime = System.currentTimeMillis();
		totalPauseTime += (currentTime - startOfPauseTime);
		startTimeAndPauseTime = START_TIME + totalPauseTime;

		END_TIME = currentTime;
	}


	//
	// Getters
	//

	public long getStartTimeDate() {
		if (master.getTrackerStatus() == SessionTrackerService.STATUS_BEFORE) {
			throw new IllegalStateException("TimeTracker.getStartTime() - invalid state when called, tracking not started.");
		}
		return START_TIME;
	}

	public long getRuntimeDurationMs() {
		int status = master.getTrackerStatus();
		switch (status){

		case SessionTrackerService.STATUS_BEFORE:
			return 0;

		case SessionTrackerService.STATUS_RUNNING:
			return  System.currentTimeMillis() - startTimeAndPauseTime;

		case SessionTrackerService.STATUS_PAUSED:
			return startOfPauseTime - startTimeAndPauseTime;

		case SessionTrackerService.STATUS_AFTER:
			return END_TIME - startTimeAndPauseTime;
			
		default:
			throw new IllegalStateException("TimeTracker - getDurationMs() - currently in invalid state.");
		}
	}

	public long getPauseDurationMs() {
		int status = master.getTrackerStatus();
		switch (status){

		case SessionTrackerService.STATUS_BEFORE:
			return 0;

		case SessionTrackerService.STATUS_RUNNING:
			return  startTimeAndPauseTime - START_TIME;

		case SessionTrackerService.STATUS_PAUSED:
			return (startOfPauseTime - System.currentTimeMillis()) +  (startTimeAndPauseTime - START_TIME);

		case SessionTrackerService.STATUS_AFTER:
			return startTimeAndPauseTime - START_TIME;
			
		default:
			throw new IllegalStateException("TimeTracker - getPauseDurationMs() - currently in invalid state.");
		}
	}
}
