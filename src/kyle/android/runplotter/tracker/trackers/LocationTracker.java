package kyle.android.runplotter.tracker.trackers;

import kyle.android.runplotter.data.model.SessionData;
import kyle.android.runplotter.data.model.SessionDataPoint;
import kyle.android.runplotter.tracker.MasterTracker;
import kyle.android.runplotter.tracker.SessionTrackerService;
import kyle.android.runplotter.tracker.Tracker;
import android.content.Context;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.SystemClock;

/**
 * A Tracker of location data - gps location data {@link SessionData}, distances and current status.
 * 
 * @author Kyle Pink
 */
public class LocationTracker implements Tracker, LocationListener, GpsStatus.Listener {

	public static final int GPS_TIMEOUT_MS = 10000;

	private MasterTracker master;

	
	//
	//  Location data.
	//

	private SessionDataPoint previousLocation;
	private long previousLocationTime;
	private SessionData locations = new SessionData();
	private boolean previousIsGpsCurrent = false;
	private boolean gpsEnabled = false;
	private int currentGpsStatus;


	//
	//  Temporary data for when new data is retrieved.
	//

	private float[] newDistance = new float[1];

	private double totalDistanceMetres = 0;
	private float currentSpeedMps = 0;

	public LocationTracker(MasterTracker masterTracker) {
		this.master = masterTracker;
	}

	@Override
	public void onCreate() {
		//Registers self as listener.
		LocationManager locationProvider = (LocationManager) master.getAppContext().getSystemService(Context.LOCATION_SERVICE);
		locationProvider.requestLocationUpdates(LocationManager.GPS_PROVIDER, 4000, 5, this);
		locationProvider.addGpsStatusListener(this);
		gpsEnabled = locationProvider.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	@Override
	public void onDestroy() {
		//Removes location/gps updates.
		LocationManager locationProvider = (LocationManager) master.getAppContext().getSystemService(Context.LOCATION_SERVICE);
		locationProvider.removeGpsStatusListener(this);
		locationProvider.removeUpdates(this);
	}


	//
	//  Transitions
	//

	@Override
	public void performTransStart() {
		addLocation(SessionTrackerService.STATUS_BEFORE, master.getDurationMs());
		addLocation(SessionTrackerService.STATUS_RUNNING, master.getDurationMs());
	}

	@Override
	public void performTransPause() {
		addLocation(SessionTrackerService.STATUS_RUNNING, master.getDurationMs());
		addLocation(SessionTrackerService.STATUS_PAUSED, master.getDurationMs());
	}

	@Override
	public void performTransResume() {
		addLocation(SessionTrackerService.STATUS_PAUSED, master.getDurationMs());
		addLocation(SessionTrackerService.STATUS_RUNNING, master.getDurationMs());
	}

	@Override
	public void performTransRunningStop() {
		addLocation(SessionTrackerService.STATUS_RUNNING, master.getDurationMs());
		addLocation(SessionTrackerService.STATUS_AFTER, master.getDurationMs());
	}

	@Override
	public void performTransPausedStop() {
		addLocation(SessionTrackerService.STATUS_PAUSED, master.getDurationMs());
		addLocation(SessionTrackerService.STATUS_AFTER, master.getDurationMs());
	}


	//
	//  Getters
	//

	public int getGpsProviderStatus() {
		if (gpsEnabled) {
			if (isGpsCurrent()) {
				return LocationProvider.AVAILABLE;
			} else {
				return LocationProvider.TEMPORARILY_UNAVAILABLE;
			}

		} else {
			return LocationProvider.OUT_OF_SERVICE;
		}
	}

	public double getTotalDistanceMetres() {
		return totalDistanceMetres;
	}

	public float getCurrentSpeedMps() {
		return currentSpeedMps;
	}

	public SessionData getSessionData() {
		return locations;
	}


	//
	//  Helpers
	//

	private void addLocation(int status, long duration) {
		locations.addData(createLocation(status, duration, previousLocation));
	}

	private SessionDataPoint createLocation(int status, long duration, SessionDataPoint locationData) {
		if (locationData == null) {
			return new SessionDataPoint(status, duration);
		} else {
			return new SessionDataPoint(status, duration, locationData.getLat(), locationData.getLon(), locationData.getAlt());
		}
	}


	//
	//  Location listening methods.
	//

	@Override
	public void onLocationChanged(Location location) {
		//Retrieves data
		currentSpeedMps = location.getSpeed();
		int status = master.getTrackerStatus();
		long duration = master.getDurationMs();

		//creates points
		SessionDataPoint newLocation = new SessionDataPoint(status, duration, location.getLatitude(), location.getLongitude(), location.getAltitude());
		SessionDataPoint oldLocation = previousLocation;

		//prev data
		previousLocation = newLocation;
		previousLocationTime = SystemClock.elapsedRealtime();

		//Skips all if session not started
		if (status != SessionTrackerService.STATUS_RUNNING && status != SessionTrackerService.STATUS_PAUSED) {
			return;
		}

		locations.addData(newLocation);

		//Adds/calculates distance if RUNNING and valid locations
		if (status == SessionTrackerService.STATUS_RUNNING && oldLocation != null && !oldLocation.isNull()){
			Location.distanceBetween(oldLocation.getLat(), oldLocation.getLon(), newLocation.getLat(), newLocation.getLon(), newDistance);
			totalDistanceMetres = totalDistanceMetres + newDistance[0];
		}

		master.sendBroadcastUpdate();
	}	


	@Override
	public void onProviderDisabled(String provider) {
		gpsEnabled = false;
	}

	@Override
	public void onProviderEnabled(String provider) {
		gpsEnabled = true;
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		//Nothing here, not functional as expected for some versions(if not all?).
	}


	//
	//  GPS status listening methods
	//

	@Override
	public void onGpsStatusChanged(int event) {
		currentGpsStatus = event;
	}

	private boolean isGpsCurrent() {
		boolean gpsCurrent = previousIsGpsCurrent;
		
		switch (currentGpsStatus) {
		
		case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
			if (previousLocation != null)
				gpsCurrent = (SystemClock.elapsedRealtime() - previousLocationTime) < GPS_TIMEOUT_MS;
			break;

		case GpsStatus.GPS_EVENT_FIRST_FIX:
			gpsCurrent = true;
			break;
		}
		
		previousIsGpsCurrent = gpsCurrent;
		
		return gpsCurrent;
	}
}
