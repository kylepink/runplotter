package kyle.android.runplotter.tracker.trackers;

import kyle.android.runplotter.tracker.MasterTracker;
import kyle.android.runplotter.tracker.Tracker;

/**
 * A {@link Tracker} which tracks steps taken within a session.
 * 
 * @author Kyle Pink
 */
public class StepTracker implements Tracker {

	private MasterTracker master;
	
	private long steps;
	
	public StepTracker(MasterTracker masterTracker) {
		this.master = masterTracker;
	}

	@Override
	public void onCreate() {
	}

	@Override
	public void onDestroy() {

	}
	
	
	//
	//  Transitions
	//

	@Override
	public void performTransStart() {
		
	}

	@Override
	public void performTransPause() {
		
	}

	@Override
	public void performTransResume() {
		
	}

	@Override
	public void performTransRunningStop() {
		
	}

	@Override
	public void performTransPausedStop() {
		
	}
	
	
	//
	//  Getters
	//
	
	public long getSteps() {
		return steps;
	}
}
