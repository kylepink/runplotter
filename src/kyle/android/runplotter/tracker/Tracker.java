package kyle.android.runplotter.tracker;

import kyle.android.runplotter.util.Component;

/**
 * A Tracker interface defines a tracker object which tracks something related
 * to the {@link SessionTrackerService}.
 * 
 * @author Kyle Pink
 */
public interface Tracker extends Component, StateTransitioner { }
