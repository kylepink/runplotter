package kyle.android.runplotter.tracker;

import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.DataSaver;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.data.model.SessionType;
import kyle.android.runplotter.data.model.TrackingType;
import kyle.android.runplotter.ui.main.SessionActivity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * TrackerService which tracks sessions and its related data.
 * 
 * @author Kyle Pink
 */
public class SessionTrackerService extends Service {

	//Tracker status.
	public static final int STATUS_BEFORE = 1;
	public static final int STATUS_RUNNING = 2;
	public static final int STATUS_PAUSED = 3;
	public static final int STATUS_AFTER = 4;


	//
	//  Incoming commands
	//

	/** Intents action string for sending this service commands */
	public static final String INTENT_ACTION_COMMAND = "kyle.android.runplotter.service.TrackerService.command";

	/** Value to retrieve on command */
	public static final String COMMAND_TYPE = "command_type";

	//Tracker commands.
	/** Starts a run from a BEFORE status */
	public static final int COMMAND_START = 1;

	/** Pauses a run from a RUNNING status */
	public static final int COMMAND_PAUSE = 2;

	/** Resumes run from a PAUSED status */
	public static final int COMMAND_RESUME = 3;

	/** Stops run, saves data, broadcasts ID and kills service */
	public static final int COMMAND_STOP = 4;

	//Service commands.
	/** Command to kill service with saving content */
	public static final int COMMAND_KILL_SAVE = 5;

	/** Command to kill service without saving content */
	public static final int COMMAND_KILL_DISCARD = 6;

	/** Command to send update info as broadcast */
	public static final int COMMAND_SEND_UPDATE = 7;


	//
	//  Constants for outgoing broadcasts.
	//
		
	//Action - for send and receiving update broadcast
	public static final String INTENT_ACTION_UPDATE = "kyle.android.runplotter.service.TrackerService.update";
	
	//Keys to retrieve data from intent:
	//data types: int, int, float, float, long, float.
	public static final String UPDATE_DATA_INFO_STATUS = "status";
	
	public static final String UPDATE_DATA_INFO_GPS_PROVIDER_STATUS = "gps";

	public static final String UPDATE_DATA_INFO_TOTAL_DIST_M = "dist";

	public static final String UPDATE_DATA_INFO_SPEED_MPS = "speed";

	public static final String UPDATE_DATA_INFO_TIME = "time";

	public static final String UPDATE_DATA_INFO_SPEED_AVG_MPS = "avg_speed";
	

	//Action - for send and receiving transition broadcast.
	public static final String INTENT_ACTION_TRANSITION = "kyle.android.runplotter.service.TrackerService.complete";
	
	//Keys
	//data types: int(TRANSITION_TYPE_*), long
	public static final String TRANSITION_TYPE = "transition_type";
	public static final String TRANSITION_SESSION_SAVE_ID = "session_id";
	
	//TRANSITION_TYPE Values
	public static final int TRANSITION_TYPE_SESSION_FINISH_SAVE = 1;
	public static final int TRANSITION_TYPE_SESSION_FINISH_DISCARD = 2;

	
	//
	//  Static access methods.
	//
	
	/**
	 * Returns the result if there is an instance of the Tracker service
	 * currently running.
	 * 
	 * @param context - context to obtain checks if there is instance of this service.
	 * @return returns true or false to indicate if there is an existing TrackerService running.
	 */
	public static boolean isTrackerServiceRunning(Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (SessionTrackerService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}


	//
	//  Data for tracking service.
	//

	private MasterTracker tracker;
	private CommandReciever commandReciever;
	private LocalBroadcastManager localBroadcaster;

	@Override
	public void onCreate() {
		Log.i(getClass().getName(),"Service created.");

		//Creates Notification object to be used for the service.
		Notification foregroundNotification = new Notification(android.R.drawable.ic_menu_view,"Run session in progress.", System.currentTimeMillis());

		//Creates new intent for launching the activity.
		Intent intent = new Intent(this, SessionActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		//Gets pending intent.
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

		//Sets notification setting info.
		foregroundNotification.setLatestEventInfo(this, "RunPlotter Session", "Now in progress..", pendingIntent);
		foregroundNotification.flags |= Notification.FLAG_NO_CLEAR;

		//Puts service in foreground. - prevent being killed off.
		startForeground(1, foregroundNotification);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Loads data for new tracker.
		SessionType sessionType;
		TrackingType trackingType;
		Object trackingTypeData;

		Bundle bundle = intent.getExtras();

		DataLoader db = ((RunPlotterApplication) getApplicationContext()).getDataLoader();
		db.open();
		sessionType = db.getSessionType(bundle.getLong(RunPlotterApplication.INTENT_SESSION_TYPE_ID));
		db.close();
		db = null;

		//Retrieves tracking type data
		trackingType = TrackingType.values()[bundle.getInt(RunPlotterApplication.INTENT_SESSION_TRACKING_TYPE)];
		switch(trackingType) {
		case DISTANCE_TARGET:
			trackingTypeData = Double.valueOf(bundle.getDouble(RunPlotterApplication.INTENT_SESSION_TRACKING_TYPE_TARGET_DISTANCE));
			break;
			
		case TIME_TARGET:
			trackingTypeData = Long.valueOf(bundle.getLong(RunPlotterApplication.INTENT_SESSION_TRACKING_TYPE_TARGET_TIME));
			break;
			
		case FREE_TRACKING:
			trackingTypeData = null;
			break;
			
		default:
			throw new IllegalStateException("SessionTrackerService.onStartCommand() - invalid tracking type.");
			
		}
		
		//Creates tracker.
		tracker = new MasterTracker(this, sessionType, trackingType, trackingTypeData);
		tracker.onCreate();

		//Registers receiver locally.
		localBroadcaster = LocalBroadcastManager.getInstance(this);
		commandReciever = new CommandReciever();
		localBroadcaster.registerReceiver(commandReciever, new IntentFilter(INTENT_ACTION_COMMAND));

		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Log.i(getClass().getName(), "Service destroyed.");
		
		tracker.onDestroy();
		
		//Removes broadcast updates, and stops foreground notification.
		localBroadcaster.unregisterReceiver(commandReciever);
		stopForeground(true);
	}

	@Override
	public void onLowMemory(){
		//TODO redundant pause data clean?
	}


	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	

	//
	//  Command broadcaster receiver.
	//

	public class CommandReciever extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			//Retrieves command, -1 = null/no command.
			int command = intent.getIntExtra(COMMAND_TYPE, -1);
			
			Log.w(SessionTrackerService.this.getClass().getName(), String.valueOf(command));

			switch (command) {
			case COMMAND_START:
				tracker.setStatus(SessionTrackerService.STATUS_RUNNING);
				break;
				
			case COMMAND_PAUSE:
				tracker.setStatus(SessionTrackerService.STATUS_PAUSED);
				break;
				
			case COMMAND_RESUME:
				tracker.setStatus(SessionTrackerService.STATUS_RUNNING);
				break;
				
			case COMMAND_STOP:
				tracker.setStatus(SessionTrackerService.STATUS_AFTER);
				break;

			case COMMAND_KILL_SAVE:
				//Checks that state is after.
				if (tracker.getTrackerStatus() != SessionTrackerService.STATUS_AFTER) {
					throw new IllegalStateException(this.getClass().getName() + " - invalid state of tracker when COMMAND_KILL_ISSUED.");
				}
				
				new AsyncTask<String, String, String>() {

					@Override
					protected String doInBackground(String... params) {
						//Retrieves and saves Session.
						Session session = tracker.getEndRunSession();
						
						DataSaver db = ((RunPlotterApplication) getApplicationContext()).getDataSaver();
						db.open();
						db.addSession(session);
						db.close();
						db = null;
						
						//Broadcasts update for end of session.
						Intent saveBroadcastIntent = new Intent();
						
						saveBroadcastIntent.setAction(INTENT_ACTION_TRANSITION);
						saveBroadcastIntent.putExtra(TRANSITION_TYPE, TRANSITION_TYPE_SESSION_FINISH_SAVE);
						saveBroadcastIntent.putExtra(TRANSITION_SESSION_SAVE_ID, session.getId());
						localBroadcaster.sendBroadcast(saveBroadcastIntent);
						
						stopSelf();
						return null;
					}
				}.execute();
				break;

			case COMMAND_KILL_DISCARD:
				//Broadcasts update for end of session.
				Intent killBroadcastIntent = new Intent();
				
				killBroadcastIntent.setAction(INTENT_ACTION_TRANSITION);
				killBroadcastIntent.putExtra(TRANSITION_TYPE, TRANSITION_TYPE_SESSION_FINISH_DISCARD);
				localBroadcaster.sendBroadcast(killBroadcastIntent);
				
				stopSelf();
				break;
				
			case COMMAND_SEND_UPDATE:
				tracker.sendBroadcastUpdate();
				break;
				
			default:
				throw new IllegalStateException("SessionTrackerService - Invalid command recieved from intent.");
			}
		}
	}
}
