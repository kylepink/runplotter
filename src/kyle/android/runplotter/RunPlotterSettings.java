package kyle.android.runplotter;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * Settings class for the Run Plotter application.
 * Encapsulates shared preferences for the application.
 * 
 * Should only be received from the RunPlotter Application 
 * 
 * @author Kyle Pink
 */
public class RunPlotterSettings {

	public static final int PAUSE_SESSION_ON_CALL_RING = 1;
	public static final int PAUSE_SESSION_ON_CALL_ANSWER = 2;

	//Context for preference access.
	private Context applicationContext;
	private SharedPreferences prefs;

	/**
	 * Only initiated by the RunplotterApplication.
	 * 
	 * @param applicationContext - Application context to prevent possible leakage.
	 * @oaram readAgain - Attempts to find new default values to set if not set.
	 */
	protected RunPlotterSettings(Context applicationContext) {
		this.applicationContext = applicationContext;

		loadSettings(false);
	}
	
	
	//
	//  Modifiers for the settings.
	//

	public void setSettingsToDefault() {
		//Retrieves old values
		int oldPrevDatabaseVer = prefs.getInt("prevDatabaseVersion", 0);
		boolean oldFirstTime = prefs.getBoolean("firstTime", true);
		
		//Empties prefs with the two old values.
		Editor editor = prefs.edit();
		editor.clear();
		editor.putInt("prevDatabaseVersion", oldPrevDatabaseVer);
		editor.putBoolean("firstTime", oldFirstTime);
		editor.commit();
		
		loadSettings(true);
	}

	private void loadSettings(boolean readAgain){
		prefs = PreferenceManager.getDefaultSharedPreferences(this.applicationContext);
		PreferenceManager.setDefaultValues(applicationContext, R.xml.runplotter_settings, readAgain);
	}
	
	public void setVersionsAndFirstTimeFlag(int databaseVersion) {
		//TODO APP VERSION?
		//Retrieves editor.
		SharedPreferences.Editor editor = prefs.edit();
		
		//Sets/stores data.
		editor.putBoolean("firstTime", false);
		editor.putInt("prevDatabaseVersion", databaseVersion);
		
		editor.commit();
	}


	//
	//  Getters
	//

	//Back end only settings - invisible to users.
	public boolean isFirstTime() {
		return prefs.getBoolean("firstTime", true);
	}

	public int getPrevDatabaseVersion() {
		return prefs.getInt("prevDatabaseVersion", 0);
	}
	
	
	//
	//  Actual settings
	//
	
	//Metric settings.
	public int getDistanceMeasurementType() {
		return Integer.parseInt(prefs.getString("distanceMeasurementType", "1"));
	}

	public int getEnergyMeasurementType() {
		return Integer.parseInt(prefs.getString("energyMeasurementType", "1"));
	}
	
	//Session audio settings.
	public boolean isSessionAudio() {
		return prefs.getBoolean("sessionAudio", false);
	}

	public int getAudioAnnounceFrequency() {
		return Integer.parseInt(prefs.getString("audioAnnounceFrequency", "5"));
	}
	
	//Tracking settings
	public boolean isStopSessionOnTarget() {
		return prefs.getBoolean("stopSessionOnTarget", false);
	}

	//Inactivity Settings
	public boolean isInactivityChecker() {
		return prefs.getBoolean("inactivityChecker", false);
	}
	
	public boolean isMaxTime() {
		return prefs.getBoolean("maxTime", false);
	}

	public int getMaxRunTimeHrs() {
		return Integer.parseInt(prefs.getString("maxRunTimeHrs", "5"));
	}
	
	public boolean isNotifyInactivityMaxTimeActivation() {
		return prefs.getBoolean("notifyInactivityMaxTimeActivation", false);
	}
	
	public boolean isSaveSessionOnInactivityMaxTime() {
		return prefs.getBoolean("saveSessionOnInactivityMaxTime", false);
	}
	
	//Call Settings
	public boolean isPauseOnCall() {
		return prefs.getBoolean("pauseOnCall", false);
	}

	public int getPauseOnCallWhen() {
		return Integer.parseInt(prefs.getString("pauseOnCallWhen", "1"));
	}

	public boolean isResumeAfterCall() {
		return prefs.getBoolean("resumeAfterCall", false);
	}

	public boolean isSameForOutgoingCalls() {
		return prefs.getBoolean("sameForOutgoingCalls", false);
	}
}
