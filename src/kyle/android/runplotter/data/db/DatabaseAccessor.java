package kyle.android.runplotter.data.db;

import kyle.android.runplotter.data.DataAccessor;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * DatabaseLoader allows the opening
 * of the RunPlotter data repository.
 * 
 * @see DataAccessor
 * @see DatabaseLoader
 * @see DatabaseSaver
 * @author Kyle Pink
 */
public class DatabaseAccessor implements DataAccessor {

	private RunPlotterSQLiteHelper sqlHelper;
	private SQLiteDatabase db;

	public DatabaseAccessor(Context context) {
		sqlHelper = new RunPlotterSQLiteHelper(context);
	}

	@Override
	public void open() throws IllegalStateException {
		if (isOpen()) {
			throw new IllegalStateException("Database already open.");
		}
		
		//opens db
		db = sqlHelper.getWritableDatabase();
	}

	@Override
	public void close() throws IllegalStateException {
		if (!isOpen()) {
			throw new IllegalStateException("Database already closed.");
		}
		
		//closes db
		db = null;
		sqlHelper.close();
	}

	@Override
	public boolean isOpen() {
		return db != null;
	}
	
	protected SQLiteDatabase getDb() throws IllegalStateException {
		if (!isOpen())
			throw new IllegalStateException("Database has not been opened.");
		else
			return db;
	}
}