package kyle.android.runplotter.data.db;

import java.util.ArrayList;

import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.model.Label;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.data.model.SessionData;
import kyle.android.runplotter.data.model.SessionDataPoint;
import kyle.android.runplotter.data.model.SessionSummaryInfo;
import kyle.android.runplotter.data.model.SessionType;
import kyle.android.runplotter.data.model.TrackingType;
import android.content.Context;
import android.database.Cursor;

/**
 * DatabaseLoader allows the loading
 * of data from the RunPlotter repository.
 * 
 * @see DataLoader
 * @author Kyle Pink
 */
//TODO handle non existent objects better if no result exist.
public class DatabaseLoader extends DatabaseAccessor implements DataLoader {

	/**
	 * Only constructor which loads the database instance to load data.
	 * 
	 * @param context - an Android context.
	 */
	public DatabaseLoader(Context context) {
		super(context);
	}

	//
	//  Label loading methods - get Label(s):
	//    single by labelId, all, many by sessionId
	//

	@Override
	public Label getLabel(long labelId) {
		Cursor retCursor;
		Label retLabel;

		//Performs Query
		String selection = RunPlotterSQLiteHelper.LABEL_COLUMN_ID + " = ?";
		String[] selectionArgs = new String[]{String.valueOf(labelId)};
		retCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_LABEL, null, selection, selectionArgs, null, null, null);

		//Returns null if no label result exists.
		if (!retCursor.moveToFirst()) {
			retCursor.close();
			return null;
		}

		retLabel = cursorToLabel(retCursor);

		retCursor.close();
		return retLabel;
	}


	@Override
	public ArrayList<Label> getAllLabels() {
		Cursor retCursor;
		ArrayList<Label> retList = new ArrayList<Label>();

		//Performs Query.
		retCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_LABEL, null, null, null, null, null, null);

		//Returns empty arrayList if no label result exists.
		if (!retCursor.moveToFirst()) {
			retCursor.close();
			return retList;
		}

		//Cycles over cursor adding each Label to the list.
		while (!retCursor.isAfterLast()) {
			retList.add(cursorToLabel(retCursor));
			retCursor.moveToNext();
		}

		retCursor.close();
		return retList;
	}


	@Override
	public ArrayList<Label> getSessionLabels(long sessionId) {
		Cursor labelCursor;
		ArrayList<Label> dataList = new ArrayList<Label>();

		//Performs db query for labels.
		String query = "SELECT * FROM "+ RunPlotterSQLiteHelper.TABLE_LABEL +" label INNER JOIN " +
				RunPlotterSQLiteHelper.TABLE_SESSION_LABEL + " session_label ON session_label."+ RunPlotterSQLiteHelper.SESSION_LABEL_COLUMN_LABELID +
				" = label."+ RunPlotterSQLiteHelper.LABEL_COLUMN_ID +
				" WHERE session_label."+ RunPlotterSQLiteHelper.SESSION_LABEL_COLUMN_SESSIONID +" = ?";
		String[] queryArgs = new String[]{String.valueOf(sessionId)};
		labelCursor = getDb().rawQuery(query, queryArgs);

		//Returns no labels if no result exists.
		if (!labelCursor.moveToFirst()) {
			labelCursor.close();
			return dataList;
		}

		//Cycles over cursor adding each Label to the list.
		while (!labelCursor.isAfterLast()) {
			dataList.add(cursorToLabel(labelCursor));
			labelCursor.moveToNext();
		}

		labelCursor.close();
		return dataList;
	}

	private Label cursorToLabel(Cursor labelCursor) {
		return new Label(labelCursor.getLong(0), labelCursor.getString(1));
	}


	//
	//  SessionType loading methods get SessionType(s):
	//    single by name or id, all.
	//

	@Override
	public SessionType getSessionType(String name) {
		Cursor retCursor;
		SessionType retSession;

		//Query
		String[] columns = new String[]{RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_ID, RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_TRACKER_MASK};
		String selection = RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_NAME + " = ?";
		String[] selectionArgs = new String[]{name};

		retCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_SESSION_TYPE, columns,
				selection, selectionArgs, null, null, null);

		//Return -1 if no result.
		if (!retCursor.moveToFirst()) {
			retCursor.close();
			return null;
		}

		retSession = new SessionType(retCursor.getLong(0), name, retCursor.getInt(1));

		retCursor.close();
		return retSession;
	}


	@Override
	public SessionType getSessionType(long sessionTypeId) {
		Cursor retCursor;
		SessionType retSessionType;

		//Performs query.
		String[] columns = new String[]{RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_NAME, RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_TRACKER_MASK};
		String selection = RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_ID + " = ?";
		String[] selectionArgs = new String[]{String.valueOf(sessionTypeId)};

		retCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_SESSION_TYPE, columns, selection, selectionArgs, null, null, null);

		//Return null if no result.
		if (!retCursor.moveToFirst()) {
			retCursor.close();
			return null;
		}

		retSessionType = new SessionType(sessionTypeId, retCursor.getString(0), retCursor.getInt(1));

		retCursor.close();
		return retSessionType;
	}


	@Override
	public ArrayList<SessionType> getAllSessionTypes() {
		Cursor retCursor;
		ArrayList<SessionType> retSessionTypes = new ArrayList<SessionType>();

		//The query.
		retCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_SESSION_TYPE, null, null, null, null, null, null);

		//Return null if no result.
		if (!retCursor.moveToFirst()) {
			retCursor.close();
			return retSessionTypes;
		}

		//Cycle through cursor adding items to result.
		while(!retCursor.isAfterLast()) {
			retSessionTypes.add(new SessionType(retCursor.getInt(0), retCursor.getString(1), retCursor.getInt(2)));
			retCursor.moveToNext();
		}

		retCursor.close();
		return retSessionTypes;
	}


	//
	//  Session loading methods, get Session-:
	//    single Session by sessionID, single SessionInfo by sessionID, many SessionInfo of tracking type.
	//

	//Via sessionID
	@Override
	public Session getSession(long sessionID) {
		Cursor sessionCursor;
		Session retSession;

		//Return data that will make the session to be returned.
		long retDate;
		int retSessionTypeId;
		TrackingType retTrackingType;
		Object retTrackingTypeData;
		long retDuration;
		long retPauseDuration;
		double retDistance;
		long retSteps;
		long retEnergy;
		SessionType sessionType;
		SessionData sessionData;
		ArrayList<Label> appliedLabels;

		//Performs query.
		String selection = RunPlotterSQLiteHelper.SESSION_COLUMN_ID + " = ?";
		String[] selectionArgs = new String[]{String.valueOf(sessionID)};
		sessionCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_SESSION, null, selection, selectionArgs, null, null, null);

		//Returns null if no result;
		if (!sessionCursor.moveToFirst()) {
			sessionCursor.close();
			return null;
		}

		//Sets data from query.
		retDate = sessionCursor.getLong(1);
		retSessionTypeId = sessionCursor.getInt(2);
		retTrackingType = TrackingType.values()[sessionCursor.getInt(3)];
		retDuration = sessionCursor.getLong(4);
		retPauseDuration = sessionCursor.getLong(5);
		retDistance = sessionCursor.getDouble(6);
		retSteps = sessionCursor.getLong(7);
		retEnergy = sessionCursor.getLong(8);

		sessionCursor.close();

		//Retrieves other associated data for session.
		sessionType = getSessionType(retSessionTypeId);
		retTrackingTypeData = loadTrackingTypeData(sessionID, retTrackingType);
		sessionData = getSessionData(sessionID);
		appliedLabels = getSessionLabels(sessionID);

		//Creates result and returns
		retSession = new Session(sessionID, retDate, sessionType, retTrackingType, retTrackingTypeData,
				retDuration, retPauseDuration, retDistance, retSteps, retEnergy, sessionData, appliedLabels);

		return retSession;
	}


	@Override
	public SessionSummaryInfo getSessionInfo(long sessionID) {
		Cursor sessionCursor;
		SessionSummaryInfo retInfo;

		//Performs query
		String selection = RunPlotterSQLiteHelper.SESSION_COLUMN_ID + " = ?";
		String[] selectionArgs = new String[]{String.valueOf(sessionID)};
		sessionCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_SESSION, null, selection, selectionArgs, null, null, null);

		//Returns empty arrayList if no result.
		if (!sessionCursor.moveToFirst()) {
			sessionCursor.close();
			return null;
		}

		//converts query result into object.
		retInfo = cursorToSummaryInfo(sessionCursor);
		sessionCursor.close();

		return retInfo;
	}

	@Override
	public ArrayList<SessionSummaryInfo> getAllSessionInfo(TrackingType trackingType, String sortingField) {
		Cursor sessionCursor;
		ArrayList<SessionSummaryInfo> retInfo = new ArrayList<SessionSummaryInfo>();

		//Determines selection clause and sorting clause.
		String selection = (trackingType == null ? null : RunPlotterSQLiteHelper.SESSION_COLUMN_TRACKING_TYPE + " = ?");
		String[] selectionArgs = (trackingType == null ? null : new String[]{String.valueOf(trackingType.ordinal())});
		String sortClause = (sortingField == null ? null : sortingField + " ASC");

		//Performs query based on user input.
		sessionCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_SESSION, null, selection, selectionArgs, null, null, sortClause);

		//Returns empty arrayList if no session info result exists.
		if (!sessionCursor.moveToFirst()) {
			sessionCursor.close();
			return retInfo;
		}

		//Retrieves all SessionInfo from cursor.
		while (!sessionCursor.isAfterLast()) {
			retInfo.add(cursorToSummaryInfo(sessionCursor));
			sessionCursor.moveToNext();
		}

		sessionCursor.close();
		return retInfo;
	}


	private SessionSummaryInfo cursorToSummaryInfo(Cursor cursor) {
		//Sets data from query into data objects
		long sessionID = cursor.getLong(0);
		SessionType sessionType = getSessionType(cursor.getInt(2));
		ArrayList<Label> appliedLabels = getSessionLabels(sessionID);
		TrackingType trackingType = TrackingType.values()[cursor.getInt(3)];
		Object trackingTypeData = loadTrackingTypeData(sessionID, trackingType);


		return new SessionSummaryInfo(sessionID,
				cursor.getLong(1), sessionType,
				trackingType, trackingTypeData, cursor.getLong(4),
				cursor.getLong(5), cursor.getDouble(6), cursor.getLong(7), cursor.getLong(8), appliedLabels);
	}

	//
	//  TrackingTypeData loading:
	//    single by sessionId and trackingType 
	//

	//Specialities of session (via tracking type).
	//TimeTarget
	private Object loadTrackingTypeData(long sessionId, TrackingType trackingType) {

		switch(trackingType) {
		case DISTANCE_TARGET:
			return loadTrackingTypeDataDistance(sessionId);

		case TIME_TARGET:
			return loadTrackingTypeDataTime(sessionId);

		case FREE_TRACKING:
			return null;
			
		default:
			throw new IllegalStateException("DatabaseLoader.loadTrackingType(long, TrackingType) - invalid tracking type.");
		}
	}

	private Double loadTrackingTypeDataDistance(long sessionId) {
		Cursor retCursor;
		Double retTrackingTypeData;

		//Query values, and execution
		String[] columns = new String[]{RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_DISTANCE_COLUMN_DISTANCE_TARGET};
		String selection = RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_DISTANCE_COLUMN_SESSION_ID + " = ?";
		String[] selectionArgs = new String[]{String.valueOf(sessionId)};

		retCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_TRACKING_TYPE_DATA_DISTANCE, columns,
				selection, selectionArgs, null, null, null);

		//Returns null if no data.
		if (!retCursor.moveToFirst()) {
			retCursor.close();
			return null;
		}

		//returns data
		retTrackingTypeData = Double.valueOf(retCursor.getDouble(0));
		retCursor.close();

		return retTrackingTypeData;
	}

	private Long loadTrackingTypeDataTime(long sessionId) {
		Cursor retCursor;
		Long retTrackingTypeData;

		//Query
		String[] columns = new String[]{RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_TIME_COLUMN_TIME_TARGET};
		String selection = RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_TIME_COLUMN_SESSION_ID + " = ?";
		String[] selectionArgs = new String[]{String.valueOf(sessionId)};
		retCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_TRACKING_TYPE_DATA_TIME, columns, selection, selectionArgs, null, null, null);

		//Returns null if no data.
		if (!retCursor.moveToFirst()) {
			retCursor.close();
			return null;
		}

		//returns data
		retTrackingTypeData = Long.valueOf(retCursor.getLong(0));
		retCursor.close();

		return retTrackingTypeData;
	}


	//
	//  SessionData loading:
	//    single collection by sessionId
	//

	private SessionData getSessionData(long sessionID) {
		//Objects for holding and transferring data.
		SessionData retData = new SessionData();
		Cursor dataCursor;

		//Query
		String[] columns = new String[] {
				RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_STATUS,
				RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_DURATION,
				RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_LAT,
				RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_LNG,
				RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_ALT
		};
		String selection = RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_SESSIONID + " = ?";
		String[] selectionArgs = new String[]{String.valueOf(sessionID)};
		String orderBy = RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_DURATION + " ASC ";


		dataCursor = getDb().query(RunPlotterSQLiteHelper.TABLE_SESSION_DATA, columns, selection,
				selectionArgs, null, null, orderBy);


		//Returns no results if none exist.
		if (!dataCursor.moveToFirst()) {
			dataCursor.close();
			return retData;
		}

		//Retrieves each SessionDataPoint in cursor.
		while (!dataCursor.isAfterLast()) {
			retData.addData(new SessionDataPoint(dataCursor.getInt(0),dataCursor.getLong(1),
					dataCursor.getDouble(2), dataCursor.getDouble(3), dataCursor.getDouble(4)));
			dataCursor.moveToNext();
		}

		dataCursor.close();
		return retData;
	}
}
