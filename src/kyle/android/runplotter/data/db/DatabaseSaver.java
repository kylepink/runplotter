package kyle.android.runplotter.data.db;

import kyle.android.runplotter.data.DataSaver;
import kyle.android.runplotter.data.model.Label;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.data.model.SessionData;
import kyle.android.runplotter.data.model.SessionDataPoint;
import kyle.android.runplotter.data.model.SessionType;
import kyle.android.runplotter.data.model.TrackingType;
import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * DatabaseLoader allows the saving
 * persistent data to the RunPlotter repository.
 * 
 * @author Kyle Pink
 */
public class DatabaseSaver extends DatabaseAccessor implements DataSaver {

	/**
	 * Only constructor which loads the database instance to save data.
	 * 
	 * @param context - Android context.
	 */
	public DatabaseSaver(Context context) {
		super(context);
	}
	

	//  
	//  Label saving methods:
	//  add label, delete label, update label
	//  

	@Override
	public void addLabel(Label label) {
		//Checks if exists already.
		if (label.inStorage()) {
			throw new Error("DatabaseSaverError: Already marked as in database?");
		}

		//Prepare values.
		ContentValues values = new ContentValues();
		values.put(RunPlotterSQLiteHelper.LABEL_COLUMN_NAME, label.getLabel());

		//Inserts label, updates id.
		long insertID = getDb().insertOrThrow(RunPlotterSQLiteHelper.TABLE_LABEL, null, values);
		label.setId(insertID);
	}


	@Override
	public Label addLabel(String label) {		
		Label retLabel = new Label(label);
		addLabel(retLabel);
		return retLabel;
	}


	@Override
	public void updateLabel(Label label) {
		//Checks if does not exist already.
		if (!label.inStorage()) {
			throw new Error("DatabaseSaverError: Not marked as in database?");
		}

		//Prepares values.
		ContentValues values = new ContentValues();
		values.put(RunPlotterSQLiteHelper.LABEL_COLUMN_NAME, label.getLabel());
		
		getDb().update(RunPlotterSQLiteHelper.TABLE_LABEL, values, RunPlotterSQLiteHelper.LABEL_COLUMN_ID + " = " + label.getId(), null);
	}


	@Override
	public void deleteLabel(long labelID) {
		SQLiteDatabase db = getDb();
		
		//Deletes from session_label table.
		db.delete(RunPlotterSQLiteHelper.TABLE_SESSION_LABEL, RunPlotterSQLiteHelper.SESSION_LABEL_COLUMN_LABELID + " = " + labelID, null);
		//Deletes from label table.
		db.delete(RunPlotterSQLiteHelper.TABLE_LABEL, RunPlotterSQLiteHelper.LABEL_COLUMN_ID + " = " + labelID, null);
	}


	@Override
	public void deleteLabel(Label label) {
		//Checks if exists already.
		if (!label.inStorage()) {
			throw new Error("DatabaseSaverError: Not marked as in database?");
		}

		//Deletes label.
		deleteLabel(label.getId());

		//Updates id.
		label.setId(-1);
	}


	//  
	//  SessionType saving methods:
	//  add session type, update session type, delete session type
	//  

	@Override
	public void addSessionType(SessionType sessionType) {
		//Checks if exists already.
		if (sessionType.inStorage()) {
			throw new Error("DatabaseSaverError: Marked as being in database?");
		}

		//Inserts sessionType, updates id.
		long insertID = addSessionType(sessionType.getName(), sessionType.getTrackerMask());
		sessionType.setId(insertID);	
	}


	@Override
	public long addSessionType(String sessionType, int trackerMask) {
		//prepares values.
		ContentValues values = new ContentValues();
		values.put(RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_NAME, sessionType);
		values.put(RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_TRACKER_MASK, trackerMask);

		//SessionType inserted and id returned.
		long insertID = getDb().insertOrThrow(RunPlotterSQLiteHelper.TABLE_SESSION_TYPE, null, values);
		return insertID;
	}

	@Override
	public void updateSessionType(SessionType sessionType) {
		//prepares values
		ContentValues values = new ContentValues();
		values.put(RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_NAME, sessionType.getName());
		values.put(RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_TRACKER_MASK, sessionType.getTrackerMask());

		//Performs update.
		getDb().update(RunPlotterSQLiteHelper.TABLE_SESSION_TYPE, values, RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_ID + " = " + sessionType.getId(), null);
	}


	@Override
	public boolean deleteSessionType(SessionType sessionType) {
		//Checks if does not exist already.
		if (!sessionType.inStorage()) {
			throw new Error("DatabaseSaverError: Marked as not being in database?");
		}

		boolean successfullDelete = deleteSessionType(sessionType.getId());

		if(successfullDelete) {
			sessionType.setId(-1);
		}
		return successfullDelete;
	}


	@Override
	public boolean deleteSessionType(long sessionTypeId) {
		//TODO DELETE RELATED SESSION RECORDS?
		long rows = getDb().delete(RunPlotterSQLiteHelper.TABLE_SESSION_TYPE, RunPlotterSQLiteHelper.SESSION_TYPE_COLUMN_ID + " = " + sessionTypeId, null);
		return rows > 0; //though rows should always be 0 or 1
	}


	//  
	//  Session saving methods:
	//  add session, update session, delete session.
	//  

	@Override
	public void addSession(Session session) {
		//Checks if exists already.
		if (session.inStorage()) {
			throw new Error("DataManagerError: Marked as being in database?");
		}

		//Insert values.
		ContentValues values = new ContentValues();
		long insertID;
		values.put(RunPlotterSQLiteHelper.SESSION_COLUMN_DATE, session.getDate());
		values.put(RunPlotterSQLiteHelper.SESSION_COLUMN_SESSION_TYPE, session.getSessionType().getId());
		values.put(RunPlotterSQLiteHelper.SESSION_COLUMN_TRACKING_TYPE, session.getTrackingType().ordinal());
		values.put(RunPlotterSQLiteHelper.SESSION_COLUMN_DURATION, session.getDuration());
		values.put(RunPlotterSQLiteHelper.SESSION_COLUMN_PAUSE_DURATION, session.getPauseDuration());
		values.put(RunPlotterSQLiteHelper.SESSION_COLUMN_DISTANCE, session.getDistance());
		values.put(RunPlotterSQLiteHelper.SESSION_COLUMN_STEPS, session.getSteps());
		values.put(RunPlotterSQLiteHelper.SESSION_COLUMN_ENERGY, session.getEnergy());

		//Inserted
		insertID = getDb().insertOrThrow(RunPlotterSQLiteHelper.TABLE_SESSION, null, values);
		
		//Insertion of related data: TrackingTypeData, SessionData and labels.
		addTrackingTypeData(insertID, session.getTrackingType(), session.getTrackingTypeData());
		addSessionData(insertID, session.getSessionData());
		for(Label labels: session.getLabels()) {
			addLabelToSession(insertID, labels.getId());
		}

		//Updates session id.
		session.setId(insertID);
	}


	@Override
	public void updateSession(Session session) {
		//Checks if not exists already.
		if (!session.inStorage()) {
			throw new Error("DataManagerError: Marked as not being in database?");
		}

		//TODO if needed? - label updates occur elsewhere.
		throw new Error("DataManagerError: NOT YET IMPLEMENTED.");
	}


	@Override
	public void deleteSession(Session session) {
		deleteSession(session.getId());

		session.setId(-1);
	}


	@Override
	public void deleteSession(long sessionID) {
		SQLiteDatabase db = getDb();
		
		db.delete(RunPlotterSQLiteHelper.TABLE_TRACKING_TYPE_DATA_DISTANCE, RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_DISTANCE_COLUMN_SESSION_ID + " = " + sessionID, null);
		db.delete(RunPlotterSQLiteHelper.TABLE_TRACKING_TYPE_DATA_TIME, RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_TIME_COLUMN_SESSION_ID + " = " + sessionID, null);
		db.delete(RunPlotterSQLiteHelper.TABLE_SESSION_DATA, RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_SESSIONID + " = " + sessionID, null);
		db.delete(RunPlotterSQLiteHelper.TABLE_SESSION_LABEL, RunPlotterSQLiteHelper.SESSION_LABEL_COLUMN_SESSIONID + " = " + sessionID, null);
		db.delete(RunPlotterSQLiteHelper.TABLE_SESSION, RunPlotterSQLiteHelper.SESSION_COLUMN_ID + " = " + sessionID, null);
	}


	//
	//  TrackingTypeData methods:
	//  add, delete
	//

	//Specialities of session (via tracking type).
	//TimeTarget
	private void addTrackingTypeData(long sessionId, TrackingType trackingType, Object trackingTypeData) {
		ContentValues values = new ContentValues();
		
		switch(trackingType) {
		case DISTANCE_TARGET:
			values.put(RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_DISTANCE_COLUMN_SESSION_ID, sessionId);
			values.put(RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_DISTANCE_COLUMN_DISTANCE_TARGET, (Double) trackingTypeData);
			getDb().insertOrThrow(RunPlotterSQLiteHelper.TABLE_TRACKING_TYPE_DATA_DISTANCE, null, values);
			break;
			
		case TIME_TARGET:
			values.put(RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_TIME_COLUMN_SESSION_ID, sessionId);
			values.put(RunPlotterSQLiteHelper.TRACKING_TYPE_DATA_TIME_COLUMN_TIME_TARGET, (Long) trackingTypeData);
			getDb().insertOrThrow(RunPlotterSQLiteHelper.TABLE_TRACKING_TYPE_DATA_TIME, null, values);
			break;
			
		case FREE_TRACKING:
			//Nothing happens, no target data.
			break;
			
		default:
			throw new IllegalStateException("Database.addTrackingType(long, TrackingType, Object) - invalid tracking type.");
			
		}
	}
	

	//  
	//  Session_Label association saving methods:
	//  add session label, add label to session, remove label from session.
	//  

	@Override
	public void addLabelToSession(Session session, Label label) {
		//Checks if exists already.
		if (!session.inStorage()) {
			throw new Error("DataManagerError: Not marked as in database?");
		}
		
		//Adds label to session object.
		boolean addSuccess = session.addLabel(label);

		if (addSuccess) {
			//Attempts to add label to database.
			addSuccess = addLabelToSession(session.getId(), label.getId());

			//IF couldn't add to database, remove label from object.
			if (!addSuccess) {
				session.removeLabel(label);
			}
		}
	}

	private boolean addLabelToSession(long sessionID, long LabelID) {
		//Insert values.
		ContentValues values = new ContentValues();
		values.put(RunPlotterSQLiteHelper.SESSION_LABEL_COLUMN_SESSIONID, sessionID);
		values.put(RunPlotterSQLiteHelper.SESSION_LABEL_COLUMN_LABELID, LabelID);

		//Inserts values, returns success result.
		try{
			getDb().insertOrThrow(RunPlotterSQLiteHelper.TABLE_SESSION_LABEL, null, values);
			return true;
		} catch (SQLException ex) {
			return false;
		}
	}


	@Override
	public void removeLabelFromSession(Session session, Label label) {
		//Checks if exists already.
		if (!session.inStorage()) {
			throw new Error("DataManagerError: Not marked as in database?");
		}
		
		//Removes label to session object.
		boolean removeSuccess = session.removeLabel(label);

		if (removeSuccess) {
			//Attempts to remove label from database.
			removeSuccess = removeLabelFromSession(session.getId(), label.getId());

			//IF couldn't remove from database, add label back to object.
			if (!removeSuccess) {
				session.addLabel(label);
			}
		}
	}

	private boolean removeLabelFromSession(long sessionID, long LabelID) {
		//Performs deletion
		String condition = RunPlotterSQLiteHelper.SESSION_LABEL_COLUMN_SESSIONID + " = " + sessionID + " and "
				+ RunPlotterSQLiteHelper.SESSION_LABEL_COLUMN_LABELID + " = " + LabelID;
		getDb().delete(RunPlotterSQLiteHelper.TABLE_SESSION_LABEL, condition, null);

		return true; //TODO bit sill needs fixed? should probably return false in some situations?
	}


	//  
	//  session_data saving method:
	//  add session_data collection.
	//  

	private void addSessionData(long sessionID, SessionData data) {
		ContentValues values = new ContentValues();
		SQLiteDatabase db = getDb();
		
		db.beginTransaction();
		for(SessionDataPoint point : data) {
			values.put(RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_SESSIONID, sessionID);
			values.put(RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_STATUS, point.getStatus());
			values.put(RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_DURATION, point.getDuration());
			values.put(RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_LAT, point.getLat());
			values.put(RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_LNG, point.getLon());
			values.put(RunPlotterSQLiteHelper.SESSION_DATA_COLUMN_ALT, point.getAlt());

			db.insertOrThrow(RunPlotterSQLiteHelper.TABLE_SESSION_DATA, null, values);
			values.clear();			
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
}
