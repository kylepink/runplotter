package kyle.android.runplotter.data.db;

import kyle.android.runplotter.R;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * RunSQLiteHelper class facilitates the creation and updating of database 
 * within an SQLite Instance.
 * 
 * Extends the base class SQLiteOpenHelper.
 * 
 * @author Kyle Pink
 */
public class RunPlotterSQLiteHelper extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 1;	
	public static final String DATABASE_NAME = "runplotter";

	//Tables with columns.
	//label
	public static final String TABLE_LABEL = "Label";

	public static final String LABEL_COLUMN_ID = "_id";
	public static final String LABEL_COLUMN_NAME = "name";


	//session_type
	public static final String TABLE_SESSION_TYPE = "SessionType";

	public static final String SESSION_TYPE_COLUMN_ID = "_id";
	public static final String SESSION_TYPE_COLUMN_NAME = "name";
	public static final String SESSION_TYPE_COLUMN_TRACKER_MASK = "tracker_mask";

	//session
	public static final String TABLE_SESSION = "Session";

	public static final String SESSION_COLUMN_ID = "_id";
	public static final String SESSION_COLUMN_DATE = "date";
	public static final String SESSION_COLUMN_SESSION_TYPE = "sessionType";
	public static final String SESSION_COLUMN_TRACKING_TYPE = "trackingType";
	public static final String SESSION_COLUMN_DURATION = "duration";
	public static final String SESSION_COLUMN_PAUSE_DURATION = "pauseDuration";
	public static final String SESSION_COLUMN_DISTANCE = "distance";
	public static final String SESSION_COLUMN_STEPS = "steps";
	public static final String SESSION_COLUMN_ENERGY = "energy";
	
	
	//Specialities of session (via tracking type).
	//TimeTarget
	public static final String TABLE_TRACKING_TYPE_DATA_TIME = "TrackingTypeDataTime";

	public static final String TRACKING_TYPE_DATA_TIME_COLUMN_SESSION_ID = "sessionId";
	public static final String TRACKING_TYPE_DATA_TIME_COLUMN_TIME_TARGET = "timeTarget";
	
	//DistanceTarget
	public static final String TABLE_TRACKING_TYPE_DATA_DISTANCE = "TrackingTypeDataDistance";

	public static final String TRACKING_TYPE_DATA_DISTANCE_COLUMN_SESSION_ID = "sessionId";
	public static final String TRACKING_TYPE_DATA_DISTANCE_COLUMN_DISTANCE_TARGET = "distanceTarget";

	//Association tables
	//session_label
	public static final String TABLE_SESSION_LABEL = "SessionLabel";

	public static final String  SESSION_LABEL_COLUMN_SESSIONID = "sessionId";
	public static final String  SESSION_LABEL_COLUMN_LABELID = "labelId";

	//session_data
	public static final String TABLE_SESSION_DATA = "SessionData";

	public static final String SESSION_DATA_COLUMN_SESSIONID = "sessionId";
	public static final String SESSION_DATA_COLUMN_STATUS = "status";
	public static final String SESSION_DATA_COLUMN_DURATION = "duration";
	public static final String SESSION_DATA_COLUMN_LAT = "lat";
	public static final String SESSION_DATA_COLUMN_LNG = "lng";
	public static final String SESSION_DATA_COLUMN_ALT = "alt";


	//Database creation sql statements.
	//label
	public static final String CREATE_TABLE_LABEL =
			"create table " + TABLE_LABEL +"(" + 
					LABEL_COLUMN_ID + " integer primary key autoincrement," + 
					LABEL_COLUMN_NAME + " text unique not null);";


	//session_type
	public static final String CREATE_TABLE_SESSION_TYPE = 
			"create table " + TABLE_SESSION_TYPE + "(" +
					SESSION_TYPE_COLUMN_ID + " integer primary key autoincrement," + 
					SESSION_TYPE_COLUMN_NAME + " text unique not null," +
					SESSION_TYPE_COLUMN_TRACKER_MASK + " integer not null);";


	//session
	public static final String CREATE_TABLE_SESSION = 
			"create table " + TABLE_SESSION + "(" +
					SESSION_COLUMN_ID + " integer primary key autoincrement," +
					SESSION_COLUMN_DATE + " integer not null," + 
					SESSION_COLUMN_SESSION_TYPE + " integer not null references " + TABLE_SESSION_TYPE + "("+ SESSION_TYPE_COLUMN_ID + ")," +
					SESSION_COLUMN_TRACKING_TYPE + " integer not null check("+ SESSION_COLUMN_TRACKING_TYPE +" > -1  and "+ SESSION_COLUMN_TRACKING_TYPE +" < 3)," +
					SESSION_COLUMN_DURATION + " integer not null," +
					SESSION_COLUMN_PAUSE_DURATION + " integer not null," +
					SESSION_COLUMN_DISTANCE + " real not null," +
					SESSION_COLUMN_STEPS + " integer not null," +
					SESSION_COLUMN_ENERGY + " integer not null);";
	
	//Specialities of session (via tracking type).
	//TimeTarget
	private static final String CREATE_TABLE_TRACKING_TYPE_DATA_TIME =
			"create table " + TABLE_TRACKING_TYPE_DATA_TIME + "(" +
					TRACKING_TYPE_DATA_TIME_COLUMN_SESSION_ID + " integer not null references " + TABLE_SESSION + "(" + SESSION_COLUMN_ID + ")," +
					TRACKING_TYPE_DATA_TIME_COLUMN_TIME_TARGET + " integer not null);";
	
	//DistanceTarget
	private static final String CREATE_TABLE_TRACKING_TYPE_DATA_DISTANCE =
			"create table " + TABLE_TRACKING_TYPE_DATA_DISTANCE + "(" +
					TRACKING_TYPE_DATA_DISTANCE_COLUMN_SESSION_ID + " integer not null references " + TABLE_SESSION + "(" + SESSION_COLUMN_ID + ")," +
					TRACKING_TYPE_DATA_DISTANCE_COLUMN_DISTANCE_TARGET + " real not null);";

	//session_label
	public static final String CREATE_TABLE_SESSION_LABEL = 
			"create table " + TABLE_SESSION_LABEL +	"(" +
					SESSION_LABEL_COLUMN_SESSIONID + " integer not null references " + TABLE_SESSION + "(" + SESSION_COLUMN_ID + ")," + 
					SESSION_LABEL_COLUMN_LABELID + " integer not null references " + TABLE_LABEL + "(" + LABEL_COLUMN_ID + ")," +
					"primary key (" + SESSION_LABEL_COLUMN_SESSIONID + "," + SESSION_LABEL_COLUMN_LABELID + "));";


	//session_data
	public static final String CREATE_TABLE_SESSION_DATA =
			"create table " + TABLE_SESSION_DATA + "(" +
					SESSION_DATA_COLUMN_SESSIONID + " integer not null references " + TABLE_SESSION + "(" + SESSION_COLUMN_ID + ")," +
					SESSION_DATA_COLUMN_STATUS + " integer not null check (" + SESSION_DATA_COLUMN_STATUS + " > 0 and " + SESSION_DATA_COLUMN_STATUS + " < 5)," +
					SESSION_DATA_COLUMN_DURATION + " integer not null," +
					SESSION_DATA_COLUMN_LAT + " real not null," + 
					SESSION_DATA_COLUMN_LNG + " real not null," +
					SESSION_DATA_COLUMN_ALT + " real not null);";
	

	private Context appContext;

	protected RunPlotterSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		appContext = context.getApplicationContext();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//Create all tables
		db.execSQL(CREATE_TABLE_LABEL);
		db.execSQL(CREATE_TABLE_SESSION_TYPE);
		db.execSQL(CREATE_TABLE_SESSION);
		db.execSQL(CREATE_TABLE_TRACKING_TYPE_DATA_TIME);
		db.execSQL(CREATE_TABLE_TRACKING_TYPE_DATA_DISTANCE);
		db.execSQL(CREATE_TABLE_SESSION_LABEL);
		db.execSQL(CREATE_TABLE_SESSION_DATA);

		//Inserts default data.
		String[] insertData;
		
		//SessionTypes
		insertData = appContext.getResources().getStringArray(R.array.default_session_types);

		for(String type : insertData) {
			db.execSQL("INSERT INTO " + TABLE_SESSION_TYPE +
					" (" + SESSION_TYPE_COLUMN_NAME + ", " + SESSION_TYPE_COLUMN_TRACKER_MASK + ") VALUES ('" + type + "', 0);");
		}

		//Labels
		insertData = appContext.getResources().getStringArray(R.array.default_session_labels);

		for(String label : insertData) {
			db.execSQL("INSERT INTO " + TABLE_LABEL +
					" (" + LABEL_COLUMN_NAME + ") VALUES ('"+ label + "');");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//DB upgrade calls to go here.
		destroyCreateDb(db);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//DB downgrade calls to go here.
		destroyCreateDb(db);
	}

	private void destroyCreateDb(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SESSION_DATA);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SESSION_LABEL);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRACKING_TYPE_DATA_TIME);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRACKING_TYPE_DATA_DISTANCE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SESSION);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SESSION_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LABEL);
		onCreate(db);
	}
}