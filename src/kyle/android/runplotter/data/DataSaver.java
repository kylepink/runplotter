package kyle.android.runplotter.data;

import kyle.android.runplotter.data.model.Label;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.data.model.SessionType;

/**
 * Simple interface for interacting and saving persistent data for the
 * RunPlotter repository.
 * 
 * @see DataAccessor
 * @author Kyle Pink
 */
public interface DataSaver extends DataAccessor {
	
	
	//
	//  Labels
	//
	
	public Label addLabel(String label);
	
	public void addLabel(Label label);

	public void updateLabel(Label label);

	public void deleteLabel(long labelID);

	public void deleteLabel(Label label);

	
	//
	//  SessionTypes
	//
	
	public void addSessionType(SessionType sessionType);

	public long addSessionType(String sessionType, int trackerMask);

	public void updateSessionType(SessionType sessionType);

	public boolean deleteSessionType(SessionType sessionType);

	public boolean deleteSessionType(long sessionTypeId);


	//
	//  Sessions
	//
	
	public void addSession(Session session);

	public void updateSession(Session session);

	public void deleteSession(Session session);

	public void deleteSession(long sessionID);


	//
	//  Session/Label manipulators
	//
	
	public void addLabelToSession(Session session, Label label);

	public void removeLabelFromSession(Session session, Label label);

}
