package kyle.android.runplotter.data;

import java.util.ArrayList;

import kyle.android.runplotter.data.model.Label;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.data.model.SessionSummaryInfo;
import kyle.android.runplotter.data.model.SessionType;
import kyle.android.runplotter.data.model.TrackingType;

/**
 * Simple interface for retrieving persistent data from the
 * RunPlotter data repository.
 * 
 * @see DataAccessor
 * @author Kyle Pink
 */
public interface DataLoader extends DataAccessor {
	
	
	//
	//  Labels
	//
	
	public Label getLabel(long labelId);

	public ArrayList<Label> getAllLabels();

	public ArrayList<Label> getSessionLabels(long sessionID);

	
	//
	//  SessionTypes
	//
	
	public SessionType getSessionType(String name);

	public SessionType getSessionType(long sessionTypeId);

	public ArrayList<SessionType> getAllSessionTypes();

	
	//
	//  Sessions
	//
	
	public Session getSession(long sessionID);

	public SessionSummaryInfo getSessionInfo(long sessionID);

	public ArrayList<SessionSummaryInfo> getAllSessionInfo(TrackingType trackingType, String sortingField);
}
