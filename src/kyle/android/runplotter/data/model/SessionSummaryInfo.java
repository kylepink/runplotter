package kyle.android.runplotter.data.model;

import java.util.ArrayList;

/**
 * A lightweight class to represent a {@link Session} for the purposes of displaying and representing
 * Sessions. Allows for lesser amount of memory use.
 * 
 * @author Kyle Pink
 * @see Session
 */
public class SessionSummaryInfo  {

	//Descriptive Info
	private final long id;
	private final long date;
	private final SessionType sessionType;
	private final TrackingType trackingType;
	private final Object trackingTypeData;
	
	//Primary tracked info
	private final long duration;
	private final long pauseDuration;
	private final double distance;
	
	//Other tracked info.
	private final long steps;
	private final long energy;
	
	private final ArrayList<Label> labels;

	public SessionSummaryInfo(long id, long date, SessionType sessionType, TrackingType trackingType, Object trackingTypeData, long duration, long pauseDuration, double distance, long steps, long energy, ArrayList<Label> appliedLabels){
		this.id = id;
		this.date = date;
		this.sessionType = sessionType;
		this.trackingType = trackingType;
		this.trackingTypeData = trackingTypeData;

		this.duration = duration;
		this.pauseDuration = pauseDuration;
		this.distance = distance;
		
		this.steps = steps;
		this.energy = energy;
		
		this.labels = new ArrayList<Label>();
		this.labels.addAll(appliedLabels);
	}

	public boolean inStorage() {
		return id > 0;
	}
	
	
	//
	//  Getters
	//
	
	public long getId() {
		return id;
	}

	public long getDate() {
		return date;
	}

	public SessionType getSessionType() {
		return sessionType;
	}

	public TrackingType getTrackingType() {
		return trackingType;
	}
	
	public Object getTrackingTypeData() {
		return trackingTypeData;
	}

	public long getDuration() {
		return duration;
	}

	public long getPauseDuration() {
		return pauseDuration;
	}

	public double getDistance() {
		return distance;
	}

	public long getSteps() {
		return steps;
	}

	public long getEnergy() {
		return energy;
	}

	public ArrayList<Label> getLabels() {
		return labels;
	}
}
