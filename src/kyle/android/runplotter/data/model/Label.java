package kyle.android.runplotter.data.model;

/**
 * Label object which can help label, tag and categorise different sessions. Each
 * Label can be applied to multiple Sessions.
 * 
 * @author Kyle Pink
 */
public class Label {

	private long id = -1;
	private String label;

	/**
	 * Label constructor when the id of the label is known.
	 * 
	 * @param id - Unique Id which identifies the Label.
	 * @param label - String of the actual label, should also be unique.
	 */
	public Label(long id, String label){
		this.id = id;
		this.label = label;
	}

	/**
	 * Label constructor when there is currently no known id.
	 * 
	 * @param label - String of the actual label, should also be unique.
	 */
	public Label(String label){
		this.label = label;
	}

	public boolean inStorage() {
		return id > 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Label other = (Label) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString(){
		return label;
	}


	//
	//  Getters and setters
	//

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
