package kyle.android.runplotter.data.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Session data point which represents a location within {@link SessionData} collection for
 * a specific {@link Session}.
 * 
 * @see SessionData
 * @see Session
 */
public class SessionDataPoint {
	
	// Time, status and location data for the point.
	private final int status;
	private final long duration;
	private final double lat;
	private final double lon;
	private final double alt;
	
	// Google maps point for Android.
	private LatLng latLng;
	
	/**
	 * Creates a sessionDataPoint with the time and location data.
	 * 
	 * @param status - Specific status of the point.
	 * @param duration - time in milliseconds since start of run of the location.
	 * @param lat - latitude location of this point.
	 * @param lon - longitude location of this point.
	 * @param alt - altitude location of this point.
	 */
	public SessionDataPoint(int status, long duration, double lat, double lon, double alt) {
		this.status = status;
		this.duration = duration;
		this.lat = lat;
		this.lon = lon;
		this.alt = alt;
	}
	
	/**
	 * Creates a sessionDataPoint with only time data. Consider the location data to be null.
	 * 
	 * {@link isPointNull} should return true on this SessionDataPoint.
	 * 
	 * @param status - Specific status of the point.
	 * @param duration - time in milliseconds since start of run of the location.
	 */
	public SessionDataPoint(int status, long duration) {
		this.status = status;
		this.duration = duration;
		this.lat = 0;
		this.lon = 0;
		this.alt = 0;
	}	
	
	public boolean isNull() {
		return lat == 0 && lon == 0 && alt == 0;
	}
	
	
	//
	//  Getter and Setters
	//
	
	public int getStatus() {
		return status;
	}
	
	public long getDuration() {
		return duration;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLon() {
		return lon;
	}
	
	public double getAlt() {
		return alt;
	}
	
	
	//
	//  LatLng Helpers, for google maps
	//

	public LatLng getLatLng() {
		if (latLng == null) {
			latLng = new LatLng(lat, lon);
		}
		
		return latLng;
	}
}
