package kyle.android.runplotter.data.model;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Main class used to hold non persistent information about sessions.
 * Makes use of {@link DatabaseLoader} and {@link DatabaseSaver} classes which saves/loads
 * data to and from the database.
 * 
 * Storage class for sessions between saves and loads from database.
 * 
 * @author Kyle Pink
 */
public class Session {
	//Descriptive information.
	private long id = -1;
	private final long date;
	private final SessionType sessionType;
	private final TrackingType trackingType;
	private final Object trackingTypeData;

	//Primary tracked info
	private final long duration;
	private final long pauseDuration;
	private final double distance;
	
	//Other tracked info.
	private final long steps;
	private final long energy;

	//Data points in session.
	private final SessionData sessionData;

	//Currently applied labels.
	private final HashSet<Label> labels = new HashSet<Label>();

	
	/**
	 * To be used when most data is accessible, but data is yet to be saved.
	 * 
	 * Excludes label data and id.
	 * 
	 * @param date
	 * @param sessionType
	 * @param trackingType
	 * @param trackingTypeData
	 * @param duration
	 * @param pauseDuration
	 * @param distance
	 * @param steps
	 * @param energy
	 * @param sessionData
	 */
	public Session(long date, SessionType sessionType, TrackingType trackingType, Object trackingTypeData, long duration, long pauseDuration, double distance, long steps, long energy, SessionData sessionData){
		this(-1, date,sessionType, trackingType, trackingTypeData, duration, pauseDuration, distance, steps, energy, sessionData);
	}

	
	/**
	 * To be used when all data is accessible, excluding labels.
	 * 
	 * @param id
	 * @param date
	 * @param sessionType
	 * @param trackingType
	 * @param trackingTypeData
	 * @param duration
	 * @param pauseDuration
	 * @param distance
	 * @param steps
	 * @param energy
	 * @param sessionData
	 */
	public Session(long id, long date, SessionType sessionType, TrackingType trackingType, Object trackingTypeData, long duration, long pauseDuration, double distance, long steps, long energy, SessionData sessionData){
		this.id = id;
		this.date = date;
		this.sessionType = sessionType;
		this.trackingType = trackingType;
		this.trackingTypeData = trackingTypeData;

		this.duration = duration;
		this.pauseDuration = pauseDuration;
		this.distance = distance;
		
		this.steps = steps;
		this.energy = energy;

		this.sessionData = sessionData;

	}

	
	/**
	 * To be used when all data is accessible, including labels.
	 * 
	 * @param id
	 * @param date
	 * @param sessionType
	 * @param trackingType
	 * @param trackingTypeData
	 * @param duration
	 * @param pauseDuration
	 * @param distance
	 * @param steps
	 * @param energy
	 * @param sessionData
	 * @param appliedLabels
	 */
	public Session(long id, long date, SessionType sessionType, TrackingType trackingType, Object trackingTypeData, long duration, long pauseDuration, double distance, long steps, long energy, SessionData sessionData, ArrayList<Label> appliedLabels){
		this(id, date, sessionType, trackingType, trackingTypeData, duration, pauseDuration, distance, steps, energy, sessionData);

		this.labels.addAll(appliedLabels);
	}

	public boolean inStorage() {
		return id > 0;
	}

	public boolean addLabel(Label label){
		if (label == null){
			return false;
		}

		return labels.add(label);
	}

	public boolean removeLabel(Label label){
		if (label == null){
			return false;
		}

		return labels.remove(label);
	}

	public boolean contains(Label label){
		if (label == null){
			return false;
		}

		return labels.contains(label);
	}

	
	//
	//  Getters and Setters.
	//
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDate() {
		return date;
	}

	public SessionType getSessionType() {
		return sessionType;
	}

	public TrackingType getTrackingType() {
		return trackingType;
	}
	
	public Object getTrackingTypeData() {
		return trackingTypeData;
	}

	public long getDuration() {
		return duration;
	}

	public long getPauseDuration() {
		return pauseDuration;
	}

	public double getDistance() {
		return distance;
	}

	public long getSteps() {
		return steps;
	}

	public long getEnergy() {
		return energy;
	}

	public SessionData getSessionData() {
		return sessionData;
	}

	public HashSet<Label> getLabels() {
		return labels;
	}
}
