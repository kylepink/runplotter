package kyle.android.runplotter.data.model;

import java.util.Iterator;
import java.util.LinkedList;

import com.google.android.gms.maps.model.LatLngBounds;

/**
 * SessionData class represents the location data for a {@link Session}, stored as a
 * collection oriented class which stores a collection of {@link SessionDataPoint} objects.
 * 
 * @see Session
 * @see SessionDataPoint
 */
public class SessionData implements Iterable<SessionDataPoint> {

	//Internal Data
	private final LinkedList<SessionDataPoint> data = new LinkedList<SessionDataPoint>();

	//Map position helpers for framing on map
	private LatLngBounds latLngBounds = null;

	/**
	 * Creates a SessionData object with no existing data.
	 */
	public SessionData() {
	}

	/**
	 * Adds a session data point with null location for the {@link Session} in question.
	 * 
	 * @param status - status of the point when was recorded.
	 * @param duration - time since beginning of the start of session, in milliseconds.
	 */
	public void addData(int status, long duration) {
		addData(new SessionDataPoint(status, duration));
	}

	/**
	 * Adds a session data point for the {@link Session} in question.
	 * 
	 * @param status - status of the point when was recorded.
	 * @param duration - time since beginning of the start of session, in milliseconds.
	 * @param lat - latitude location value for the point.
	 * @param lng - longitude location value for the point.
	 * @param alt - altitude location value for the point.
	 */
	public void addData(int status, long duration, double lat, double lng, double alt) {
		addData(new SessionDataPoint(status, duration, lat, lng, alt));
	}

	/**
	 * Adds a SessionDataPoint for the {@link Session} in question.
	 * 
	 * @param point - data point which represents the the point's time and location data.
	 */
	public void addData(SessionDataPoint point) {
		data.add(point);
	}

	@Override
	public Iterator<SessionDataPoint> iterator() {
		return data.iterator();
	}

	public boolean isEmpty() {
		return data.isEmpty();
	}

	public int size() {
		return data.size();
	}



	//
	//  Map Positioning Helpers (framing aka span)
	//

	public LatLngBounds getLatLngBounds() {
		if (latLngBounds == null) {
			latLngBounds = createBounds();
		}

		return latLngBounds;
	}


	//Simple points bound calculator
	private LatLngBounds createBounds() {
		LatLngBounds.Builder builder= new LatLngBounds.Builder();
		boolean containsPoint = false;

		for (SessionDataPoint point : data) {
			if (!point.isNull()) {
				builder.include(point.getLatLng());
				containsPoint = true;
			}
		}
		if (containsPoint) {
			return builder.build();	
		} else {
			return null;
		}
	}
}