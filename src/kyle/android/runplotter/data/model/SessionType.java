package kyle.android.runplotter.data.model;

/**
 * SessionType represents a specific type which can be applied to class specific {@link Session}s.
 * Can be applied to multiple Sessions, each session can only have one SessionType applied to them.
 * 
 * @author Kyle Pink
 */
public class SessionType {
	
	private long id = -1;
	private String name;
	private int trackerMask;

	/**
	 * General construct for when the Id is known (already exists and stored).
	 * 
	 * @param id - Unique identifier for the SessionType
	 * @param name - Human readable name for the SessionType, ideally unique.
	 * @param trackerMask - BitMask of the features to track.
	 */
	public SessionType(long id, String name, int trackerMask){
		this.id = id;
		this.name = name;
		this.trackerMask = trackerMask;
	}

	/**
	 * General construct for when the Id is unknown (does not exists and not stored).
	 * 
	 * @param name - Human readable name for the SessionType, ideally unique.
	 * @param trackerMask - BitMask of the features to track.
	 */
	public SessionType(String name, int trackerMask){
		id = -1;
		this.name = name;
		this.trackerMask = trackerMask;
	}
	
	public boolean inStorage() {
		return id > 0;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SessionType other = (SessionType) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name;
	}
	
	
	//
	//  Getters and Setters
	//
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTrackerMask() {
		return trackerMask;
	}

	public void setTrackerMask(int trackerMask) {
		this.trackerMask = trackerMask;
	}
}
