package kyle.android.runplotter.data.model;

/**
 * TrackingType represents a specific type which can be applied to class specific {@link Session}s.
 * Is used to pre-define the specific behaviour for the Session which it is being used for.
 * 
 * @author Kyle Pink
 */
public enum TrackingType {
	
	FREE_TRACKING,
	TIME_TARGET,
	DISTANCE_TARGET;

	@Override
	public String toString(){
		switch(this){
		
		case FREE_TRACKING:
			return "Free Tracking";
			
		case TIME_TARGET:
			return "Time Target";
			
		case DISTANCE_TARGET:
			return "Distance Target";
			
		default:
			return "UNDEFINED";
			
		}
	}
}
