package kyle.android.runplotter.data;

/**
 * Stipulates methods to manage the opening and closing of data repositories
 * for runplotter.
 * 
 * @author Kyle Pink
 */
public interface DataAccessor {

	/**
	 * Opens a connection to the database, for access.
	 * 
	 * @throws IllegalStateException - thrown if connection to database is already open.
	 */
	public void open() throws IllegalStateException;

	/**
	 * Closes the existing connection to the database.
	 * 
	 * @throws IllegalStateException - thrown if connection to database is already closed.
	 */
	public void close() throws IllegalStateException;
	
	/**
	 * @return - boolean to indicate if the database connection is currently opened.
	 */
	public boolean isOpen();

}
