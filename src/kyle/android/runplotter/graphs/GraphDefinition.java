package kyle.android.runplotter.graphs;

import org.achartengine.chart.AbstractChart;

/**
 * Defines a graph to be rendered for an activity.
 * 
 * @see BaseGraphDefinition
 * @author Kyle Pink
 */
public interface GraphDefinition {

	/**
	 * Returns the generated AbstractChart object for this graphDefinition.
	 * 
	 * @return the AbstractChart graph chart to be rendered.
	 */
	AbstractChart getGraphChart();
	
	/**
	 * Retrieves the note which goes along with the specific graph/chart which 
	 * describes specific aspects of the chart, which are otherwise hidden.
	 * 
	 * If no note exists, note should return null.
	 * 
	 * @return returns a string to describe aspects of graph/chart. or null.
	 */
	String getGraphNote();
}
