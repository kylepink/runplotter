package kyle.android.runplotter.graphs;

import java.util.Collection;

import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.data.model.SessionSummaryInfo;
import kyle.android.runplotter.util.Distance;
import kyle.android.runplotter.util.Time;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.LineChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

/**
 * Simple line series graph for displaying
 * average speed for session and average speed for all sessions over time. 
 * 
 * @author Kyle Pink
 */
public class OverviewSessionSpeedsOverTime implements GraphDefinition {

	private Collection<SessionSummaryInfo> sessions;
	private GraphFormatHelper formatter;
	private int prefDistanceMeasurement;

	public OverviewSessionSpeedsOverTime(Collection<SessionSummaryInfo> timeDateAscendingSessions,
			GraphFormatHelper formatter, RunPlotterSettings settings) {
		//Setting session and preferences
		this.sessions = timeDateAscendingSessions;
		this.formatter = formatter;
		this.prefDistanceMeasurement = settings.getDistanceMeasurementType();
	}
	


	@Override
	public AbstractChart getGraphChart() {
		return new LineChart(getSeriesDataSet(), getSeriesRenderer());
	}

	@Override
	public String getGraphNote() {
		//return "Note: Sessions which have 0 distance are not counted.";
		return null;
	}

	public XYMultipleSeriesRenderer getSeriesRenderer() {
		XYMultipleSeriesRenderer series = formatter.getPreformattedXyMupltipleSeriesRenderer();
		
		//Sets titles/labels.
		series.setYTitle(String.format("Speed (%s)", Distance.getShortPerHourStringPreference(prefDistanceMeasurement)));
		series.setXTitle("Session Numer");

		//Creates xy series renderer, sets colour and added to multiple series renderer.
		XYSeriesRenderer s1 = new XYSeriesRenderer();
		XYSeriesRenderer s2 = new XYSeriesRenderer();

		formatter.formatXYRenderers(s1,s2);

		//Returns renderer with series renderers.
		series.addSeriesRenderer(s1);
		series.addSeriesRenderer(s2);
		return series;
	}

	public XYMultipleSeriesDataset getSeriesDataSet() {
		XYSeries currentSpeedSeries = new XYSeries("Speed for Session");
		XYSeries avgSpeedSeries = new XYSeries("Average Speed");

		//Calculation holders.
		double currentAvgSpeed = 0;
		double currentTotalAvgSpeed = 0;
		
		double totalOfAvergageSpeeds = 0;
		int totalCurrentSessionsCount = 0;

		for (SessionSummaryInfo session : sessions) {
			if (session.getDistance() == 0) {
				continue;
			}
			
			//Calculates speeds
			currentAvgSpeed = calculatePreferredSpeedPerHour(session.getDistance(), session.getDuration());
			totalOfAvergageSpeeds += currentAvgSpeed;
			currentTotalAvgSpeed = totalOfAvergageSpeeds / ++totalCurrentSessionsCount;
			
			//Adds point
			//Date sessionDate = new Date(session.getDate());
			currentSpeedSeries.add(totalCurrentSessionsCount, currentAvgSpeed);
			avgSpeedSeries.add(totalCurrentSessionsCount, currentTotalAvgSpeed);
		}

		//Returns dataset
		XYMultipleSeriesDataset dataSet = new XYMultipleSeriesDataset();
		dataSet.addSeries(currentSpeedSeries);
		dataSet.addSeries(avgSpeedSeries);
		
		return dataSet;
	}
	

	//
	//  Simple Conversion Helpers
	//

	private double calculatePreferredSpeedPerHour(double distanceMetres, long milliSeconds) {
		return Distance.mpsToPreferencePerHour(prefDistanceMeasurement,
				(float) (distanceMetres / Time.calculateSecs(milliSeconds)));
	}
}
