package kyle.android.runplotter.graphs;

import kyle.android.runplotter.R;

import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint.Align;

public class GraphFormatHelper {

	public static final int[] SERIES_COLOURS = new int[]{
		Color.RED,
		Color.GREEN,
		Color.BLUE,
		Color.MAGENTA
	};
	
	private Context context;
	
	public GraphFormatHelper(Context context){
		this.context = context;
	}
	
	
	//
	//  Helpers to retrieve range values.
	//
	
	public void setRendererDataRange(XYMultipleSeriesRenderer renderer, XYMultipleSeriesDataset dataset) {
		double[] panLimits = getPanLimits(dataset.getSeries());
		renderer.setPanLimits(panLimits);
	}
	
	//double[minx, maxx, miny, maxy]
	private double[] getPanLimits(XYSeries... series) {
		double minX = Double.MAX_VALUE;
		double maxX = Double.MIN_VALUE;
		double minY = Double.MAX_VALUE;
		double maxY = Double.MIN_VALUE;
		
		for (XYSeries serial : series) {
			if (serial.getMinX() < minX) {
				minX = serial.getMinX();
			}
			if (serial.getMaxX() > maxX) {
				maxX = serial.getMaxX();
			}
			if (serial.getMinY() < minY) {
				minY = serial.getMinY();
			}
			if (serial.getMaxY() > maxY) {
				maxY = serial.getMaxY();
			}
		}


		minX = (minX == Double.MAX_VALUE ? Double.MIN_VALUE : minX);
		maxX = (maxX == Double.MIN_VALUE ? Double.MAX_VALUE : maxX);
		minY = (minY == Double.MAX_VALUE ? Double.MIN_VALUE : minY);
		maxY = (maxY == Double.MIN_VALUE ? Double.MAX_VALUE : maxY);
		
		return new double[]{minX, maxX, minY, maxY};
	}
	
	
	//
	//  Pre-formatting of Renderer classes.
	//

	public XYMultipleSeriesRenderer getPreformattedXyMupltipleSeriesRenderer() {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		
		//Behaviour/positioning
		renderer.setYLabelsAlign(Align.LEFT);
		renderer.setShowGrid(true);
		renderer.setZoomButtonsVisible(true);
		renderer.setZoomEnabled(true, false);
		renderer.setPanEnabled(true, false);
		renderer.setXAxisMin(0);
		renderer.setYAxisMin(0);
		renderer.setAxisTitleTextSize(renderer.getAxisTitleTextSize() + 2);
		
		//Removing horizontal margins.
		int[] margins = renderer.getMargins();
		margins[1] = 2;
		margins[3] = 0;
		renderer.setMargins(margins);
		
		//Colours
		//bgs
		renderer.setApplyBackgroundColor(true);
		renderer.setBackgroundColor(Color.TRANSPARENT);
		renderer.setMarginsColor(Color.argb(0, 255, 255, 255));
		
		//grids
		renderer.setAxesColor(context.getResources().getColor(R.color.accent_bright));
		renderer.setGridColor(context.getResources().getColor(R.color.accent_dim));
		
		//text
		renderer.setLabelsColor(Color.WHITE);
		renderer.setXLabelsColor(Color.WHITE);
		renderer.setYLabelsColor(0, Color.WHITE);
		renderer.setChartTitle("");
		
		return renderer;
	}
	
	
	public void formatXYRenderers(XYSeriesRenderer... renderers) {
		colourSimpleSeriesRenderers(renderers);

		for (XYSeriesRenderer r : renderers) {
			r.setLineWidth(3);
			r.setFillBelowLine(true);

			int belowTheLineColour = (r.getColor() & 0x00FFFFFF) | 0x40000000;
			r.setFillBelowLineColor(belowTheLineColour);
		}
	}
	
	
	public void colourSimpleSeriesRenderers(SimpleSeriesRenderer... renderers) {
		int pos = 0;
		
		for (SimpleSeriesRenderer r : renderers) {
			r.setColor(SERIES_COLOURS[pos++]);
		}
	}
}
