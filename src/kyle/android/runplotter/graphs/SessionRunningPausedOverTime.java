package kyle.android.runplotter.graphs;

import org.achartengine.chart.AbstractChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;

import android.content.Context;

public class SessionRunningPausedOverTime implements GraphDefinition {

	public SessionRunningPausedOverTime(Context context) {
		
	}

	@Override
	public AbstractChart getGraphChart() {
		return null;
	}

	@Override
	public String getGraphNote() {
		return null;
	}

	public XYMultipleSeriesRenderer getSeriesRenderer() {
		return null;
	}

	public XYMultipleSeriesDataset getSeriesDataSet() {
		return null;
	}

}