package kyle.android.runplotter.graphs;

import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.data.model.SessionDataPoint;
import kyle.android.runplotter.tracker.SessionTrackerService;
import kyle.android.runplotter.util.Distance;
import kyle.android.runplotter.util.Time;

import org.achartengine.chart.AbstractChart;
import org.achartengine.chart.LineChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.location.Location;
import android.util.Log;

/**
 * Simple line series graph for displaying
 * current and average speed of a session over time. 
 * 
 * @see Session
 * @author Kyle Pink
 */
public class SessionSpeedOverTime implements GraphDefinition {

	//Retrieved settings
	private Session session;
	GraphFormatHelper formatter;
	private int prefDistanceMeasurement;

	public SessionSpeedOverTime(Session session,
			GraphFormatHelper formatter, RunPlotterSettings settings) {
		//Setting session and preferences
		this.session = session;
		this.formatter = formatter;
		this.prefDistanceMeasurement = settings.getDistanceMeasurementType();
	}
	


	@Override
	public AbstractChart getGraphChart() {
		XYMultipleSeriesDataset dataset = getSeriesDataSet();
		XYMultipleSeriesRenderer renderer = getSeriesRenderer();
		formatter.setRendererDataRange(renderer, dataset);
		return new LineChart(dataset, renderer);
	}

	@Override
	public String getGraphNote() {
		return null;
	}

	
	public XYMultipleSeriesRenderer getSeriesRenderer() {
		XYMultipleSeriesRenderer series = formatter.getPreformattedXyMupltipleSeriesRenderer();

		//Sets titles/labels.
		series.setYTitle(String.format("Speed (%s)", Distance.getShortPerHourStringPreference(prefDistanceMeasurement)));
		series.setXTitle("Time (minutes)");

		//Creates xy series renderer, sets colour and added to multiple series renderer.
		XYSeriesRenderer s1 = new XYSeriesRenderer();
		XYSeriesRenderer s2 = new XYSeriesRenderer();

		formatter.formatXYRenderers(s1,s2);

		//Returns renderer with series renderers.
		series.addSeriesRenderer(s1);
		series.addSeriesRenderer(s2);
		return series;
	}
	
	public XYMultipleSeriesDataset getSeriesDataSet() {
		
		//TimeSeries series = new TimeSeries("Speed");
		XYSeries currentSpeedSeries = new XYSeries("Speed");
		XYSeries avgSpeedSeries = new XYSeries("Average Speed");

		//Calculation holders.
		SessionDataPoint previousPoint = null;
		SessionDataPoint currentPoint = null;
		double newDistanceMetres = 0;
		double totalDistanceMetres = 0;
		float[] tempNewDistance = new float[1];
		double newSpeed = 0;
		double newAvgSpeed = 0;

		//Adds first point.
		currentSpeedSeries.add(0, 0);
		avgSpeedSeries.add(0, 0);


		for (SessionDataPoint tmpCurrentPoint : session.getSessionData()) {
			//Skips points if bad status, or null.
			if (tmpCurrentPoint.getStatus() != SessionTrackerService.STATUS_RUNNING) {
				currentPoint = null;
				previousPoint = null;
				continue;
			}
			if (tmpCurrentPoint.isNull()) {
				continue;
			}

			//Alternates data
			previousPoint = currentPoint;
			currentPoint = tmpCurrentPoint;

			//calculation of new distances and speeds if valid.
			if (previousPoint != null) {
				Location.distanceBetween(previousPoint.getLat(), previousPoint.getLon(),
						currentPoint.getLat(), currentPoint.getLon(), tempNewDistance);
				newDistanceMetres = tempNewDistance[0];
				totalDistanceMetres += newDistanceMetres;

				//Calculates speeds
				newSpeed = calculatePreferredSpeedPerHour(newDistanceMetres, currentPoint.getDuration() - previousPoint.getDuration());
				newAvgSpeed = calculatePreferredSpeedPerHour(totalDistanceMetres, currentPoint.getDuration());
				Log.i("newDistanceMetres", newDistanceMetres + " mtrs at " + currentPoint.getDuration() + "ms");
				Log.i("newspeed", newSpeed +" speed/H at " + currentPoint.getDuration() + "ms");
				Log.i("avgspeed", newAvgSpeed + " speed/H at " + currentPoint.getDuration() + "ms");

				//Adds data to series
				currentSpeedSeries.add(Time.calculateMins(currentPoint.getDuration()), newSpeed);
				avgSpeedSeries.add(Time.calculateMins(currentPoint.getDuration()), newAvgSpeed);
			}
		}
		
		//Returns dataset series
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
		dataset.addSeries(currentSpeedSeries);
		dataset.addSeries(avgSpeedSeries);
		return dataset;
	}


	//
	//  Simple Conversion Helpers
	//

	private double calculatePreferredSpeedPerHour(double distanceMetres, long milliSeconds) {
		return Distance.mpsToPreferencePerHour(prefDistanceMeasurement,
				(float) (distanceMetres / Time.calculateSecs(milliSeconds)));
	}
}
