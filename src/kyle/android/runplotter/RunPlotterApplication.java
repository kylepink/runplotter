package kyle.android.runplotter;

import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.DataSaver;
import kyle.android.runplotter.data.db.DatabaseLoader;
import kyle.android.runplotter.data.db.DatabaseSaver;
import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

/**
 * Simple Application class for Run Plotter which provides
 * Application wide services which include settings access,
 * db access, and constants for application wide services.
 * 
 * @author Kyle Pink
 */
public class RunPlotterApplication extends Application {

	@Override
	public void onCreate(){
		super.onCreate();
	}

	@Override
	public void onLowMemory() {
		//Minor stuff.
		clearSettings();
		
		//Major stuff.
	}

	
	//
	//  RunPlotter Constants for various intents for activity passing and other miscs.
	//
	
	public static final String INTENT_SESSION_ID = "runplotter.session.id";
	public static final String INTENT_SESSION_TYPE_ID = "runplotter.session.type";
	public static final String INTENT_SESSION_TRACKING_TYPE = "runplotter.session.tracking.type";
	public static final String INTENT_SESSION_TRACKING_TYPE_TARGET_DISTANCE = "runplotter.session.tracking.type.target.time";
	public static final String INTENT_SESSION_TRACKING_TYPE_TARGET_TIME = "runplotter.session.tracking.type.target.time";


	//
	//  Miscellaneous helpers.
	//
	
	//XXX needed?
	/**
	 * Retrieves the application's version code number as int. 
	 * 
	 */
	public int getApplicationVersion() {
		try {
			PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), 0);
			return pInfo.versionCode;
		} catch (NameNotFoundException e) {
			throw new Error(e);
		}
	}

	
	//
	//  RunPlotter settings.
	//
	
	private RunPlotterSettings settings;
	

	/**
	 * Returns a new instance of RunPlotterApplication settings, should not be held
	 * of extended periods of time where possible. Retrieve/Save settings then discard.
	 * 
	 * @return RunPlotterSettings object which contains relevant settings for application.
	 */
	public RunPlotterSettings getSettingsCheckDefaults() {
		settings = null;
		settings = new RunPlotterSettings(this);

		return settings;
	}

	/**
	 * Returns the stored instance (if available) of RunPlotterApplication settings,
	 * should not be held of extended periods of time where possible. Retrieve/Save
	 * settings then discard.
	 * 
	 * @return RunPlotterSettings object which contains relevant settings for application.
	 */
	public RunPlotterSettings getSettings() {
		//Creates settings if needed.
		if (settings == null) {
			settings = new RunPlotterSettings(this);
		}

		return settings;
	}

	public void clearSettings(){
		settings = null;
	}


	//
	//  DB retrieval.
	//
	
	public DataLoader getDataLoader() {
		return new DatabaseLoader(this);
	}

	public DataSaver getDataSaver() {
		return new DatabaseSaver(this);
	}
}
