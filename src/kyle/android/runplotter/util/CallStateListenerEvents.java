package kyle.android.runplotter.util;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/**
 * Simple extension class to {@link PhoneStateListener} which allows PhoneState changes
 * to be simplified into events, should not override {@link #onCallStateChanged(int, String)} but
 * use {@link #onCallStateEvent(int, String)}. Otherwise is the same as {@link PhoneStateListener}.
 * 
 * @author Kyle Pink
 * @see PhoneStateListener
 */
 //TODO MOVE INTO REUSABLE REPOSITORY.
public abstract class CallStateListenerEvents extends PhoneStateListener {

	//Call Events
	public static final int CALL_EVENT_INCOMING_RING = 1;
	public static final int CALL_EVENT_INCOMING_UNANSWER_OR_DROP = 2;
	public static final int CALL_EVENT_INCOMING_ANSWER = 3;
	public static final int CALL_EVENT_INCOMING_FINISH = 4;
	
	public static final int CALL_EVENT_OUTGOING_START = 5;
	public static final int CALL_EVENT_OUTGOING_FINISH = 6;
	
	
	private int currentCallState = TelephonyManager.CALL_STATE_IDLE;
	private boolean incomingCall;

	@Override
	public void onCallStateChanged(int newState, String incomingNumber) {
		
		switch(currentCallState) {
		case TelephonyManager.CALL_STATE_IDLE:
			if (newState == TelephonyManager.CALL_STATE_RINGING) {
				//Incoming call ringing
				incomingCall = true;
				onCallStateEvent(CALL_EVENT_INCOMING_RING, incomingNumber);
				
			} else if (newState == TelephonyManager.CALL_STATE_OFFHOOK) {
				//Outgoing call started
				incomingCall = false;
				onCallStateEvent(CALL_EVENT_OUTGOING_START, incomingNumber);
			}
			break;
			
		case TelephonyManager.CALL_STATE_OFFHOOK:
			//Should be only one possible new state
			if (newState == TelephonyManager.CALL_STATE_IDLE) {
				if (incomingCall) {
					//incoming call finished.
					onCallStateEvent(CALL_EVENT_INCOMING_FINISH, incomingNumber);
					
				} else {
					//outgoing call finished.
					onCallStateEvent(CALL_EVENT_OUTGOING_FINISH, incomingNumber);
				}
				
			}
			break;
			
		case TelephonyManager.CALL_STATE_RINGING:
			if (newState == TelephonyManager.CALL_STATE_IDLE) {
				//Incoming call unanswered/dropped.
				onCallStateEvent(CALL_EVENT_INCOMING_UNANSWER_OR_DROP, incomingNumber);
				
			} else if (newState == TelephonyManager.CALL_STATE_OFFHOOK) {
				//Incoming call answered
				onCallStateEvent(CALL_EVENT_INCOMING_ANSWER, incomingNumber);
			}
			break;
			
		default: //Nothing here
			break;
		}

		currentCallState = newState;
	}
	
	/**
	 * @param event - value representing change in call state, CallStateListenerEvents.CALL_EVENT_*
	 * @param number - phone number that which it is related to.
	 */
	protected abstract void onCallStateEvent(int event, String number);
}
