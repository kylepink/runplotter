package kyle.android.runplotter.util;

/**
 * Distance is a static class which allows for the conversion for distance measurements
 * into other measurements. Allows for conversions in the formats of metres, kilometres,
 * yards and miles. Metres is the default measurement format before conversion.
 * 
 * Also allows for conversion metres/H to pref/H.
 * 
 * @author Kyle Pink
 */
public class Distance {

	//Prevents initialisation.
	private Distance() {
	}

	//Measurement Types
	public static final int PREFERENCE_KILOMETRES = 1;
	public static final int PREFERENCE_METRES = 2;
	public static final int PREFERENCE_YARDS = 3;
	public static final int PREFERENCE_MILES = 4;

	//
	//  Distance conversion methods - converts into preferred.
	//

	/**
	 * Converts provided metres into preferred measurement,
	 * 
	 * @param measurePref - desired measurement preference
	 * @param metres -distance input as metres
	 * @return double - returns measurement in preferred metric.
	 */
	public static double metresToPreference(int measurePref, double metres) {
		switch (measurePref) {
		case PREFERENCE_KILOMETRES:
			return metres / 1000.0d;
		case PREFERENCE_METRES:
			return metres;
		case PREFERENCE_MILES:
			return metres / 1609.344d;
		case PREFERENCE_YARDS:
			return metres * 1.0936133d;
		default:
			throw new IllegalStateException("Distance - metresToPreference(pref, metres) - Invalid preference provided.");
		}
	}

	/**
	 * Converts provided metres per second into preferred measurement/hour.
	 * 
	 * @param measurePref - desired measurement preference
	 * @param mps - metres per second input
	 * @return double value of metres/second into preference/hour.
	 */
	public static double mpsToPreferencePerHour(int measurePref, double mps) {
		switch (measurePref) {
		case PREFERENCE_KILOMETRES:
			return mps * 3.6d;
		case PREFERENCE_METRES:
			return mps * 3600;
		case PREFERENCE_MILES:
			return mps * 2.23693629d;
		case PREFERENCE_YARDS:
			return mps * 3937.00787d;
		default:
			throw new IllegalStateException("Distance - mpsToPreferencePerHour(pref, mps) - Invalid preference provided.");
		}
	}

	//
	//  Following returns the distance preferences as strings
	//

	/**
	 * Provides the preferred distance measurement into shorthand preference.
	 * 
	 * @param measurePref - desired measurement preference
	 * @return - returns string representation of preference in short form.
	 */
	public static String getShortStringPreference(int measurePref) {
		switch (measurePref) {
		case PREFERENCE_KILOMETRES:
			return "km";
		case PREFERENCE_METRES:
			return "m";
		case PREFERENCE_MILES:
			return "mi";
		case PREFERENCE_YARDS:
			return "yd";
		default:
			throw new IllegalStateException("Distance - getShortStringPreference(pref) - Invalid preference provided.");
		}
	}
	
	/**
	 * Provides the preferred distance measurement into long hand preference with
	 * a uppercase start.
	 * 
	 * @param measurePref - desired measurement preference
	 * @return - returns string representation of preference in long form.
	 */
	public static String getLongCapitalisedStringPreference(int preference) {
		String word = getLongStringPreference(preference);
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}
	
	/**
	 * Provides the preferred distance measurement into long hand preference.
	 * 
	 * @param measurePref - desired measurement preference
	 * @return - returns string representation of preference in long form.
	 */
	public static String getLongStringPreference(int measurePref) {
		switch (measurePref) {
		case PREFERENCE_KILOMETRES:
			return "kilometres";
		case PREFERENCE_METRES:
			return "metres";
		case PREFERENCE_MILES:
			return "miles";
		case PREFERENCE_YARDS:
			return "yards";
		default:
			throw new IllegalStateException("Distance - getLongStringPreference(pref) - Invalid preference provided.");
		}
	}

	/**
	 * Provides the preferred distance measurement into shorthand
	 * preference/hour.
	 * 
	 * @param measurePref - desired measurement preference
	 * @return - returns string representation of preference/hr.
	 */
	public static String getShortPerHourStringPreference(int measurePref) {
		switch (measurePref) {
		case PREFERENCE_KILOMETRES:
			return "km/h";
		case PREFERENCE_METRES:
			return "m/h";
		case PREFERENCE_MILES:
			return "mph";
		case PREFERENCE_YARDS:
			return "yd/h";
		default:
			throw new IllegalStateException("Distance - getShortPerHourStringPreference(pref) - Invalid preference provided.");
		}
	}
}
