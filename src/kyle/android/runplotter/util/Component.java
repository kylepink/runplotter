package kyle.android.runplotter.util;

public interface Component {
	
	/**
	 * Setup and registration process for
	 * component and resources to occur.
	 */
	public void onCreate();
	
	/**
	 * Cleanup and de-registration process for
	 * component and resources to occur.
	 */
	public void onDestroy();
}
