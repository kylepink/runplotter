package kyle.android.runplotter.util;

/**
 * Simple time unit converter stored in centralised location,
 * {@link TimeUnit} would be more ideal if possible.
 * 
 * @author Kyle Pink
 */
public class Time {
	
	//prevent initialisation
	private Time(){}
	
	public static double calculateSecs(long milliseconds) {
		return milliseconds / 1000d;
	}

	public static double calculateMins(long milliseconds) {
		return milliseconds / 60000d;
	}

	public static double calculateHrs(long milliseconds) {
		return milliseconds / 3600000d;
	}
}
