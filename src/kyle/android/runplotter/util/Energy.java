package kyle.android.runplotter.util;

public class Energy {

	//Prevents Initialisation.
	private Energy(){}

	public static final int PREFERENCE_CALORIES = 1;
	public static final int PREFERENCE_KILOJULES = 2;


	public static float caloriesToPreference(int preference, float calories) {
		switch(preference) {
		case PREFERENCE_CALORIES:
			return calories;
		case PREFERENCE_KILOJULES:
			return (float) (calories * 0.004184);
		default:
			throw new IllegalStateException("Energy - caloriesToPreference(pref, cal) - Invalid preference provided.");
		}
	}

	public static String getShortStringPreference(int preference) {
		switch(preference) {
		case PREFERENCE_CALORIES:
			return "cl";
		case PREFERENCE_KILOJULES:
			return "kj";
		default:
			throw new IllegalStateException("Energy - getShortStringPreference(pref) - Invalid preference provided.");
		}
	}
	
	public static String getLongCapitalisedStringPreference(int preference) {
		String word = getLongStringPreference(preference);
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}
	
	public static String getLongStringPreference(int preference) {
		switch(preference) {
		case PREFERENCE_CALORIES:
			return "calories";
		case PREFERENCE_KILOJULES:
			return "kilojules";
		default:
			throw new IllegalStateException("Energy - getLongStringPreference(pref) - Invalid preference provided.");
		}
	}
	
	public static String getShortPerHourStringPreference(int preference) {
		switch(preference) {
		case PREFERENCE_CALORIES:
			return "cl/hr";
		case PREFERENCE_KILOJULES:
			return "kj/hr";
		default:
			throw new IllegalStateException("Energy - getShortPerHourStringPreference(pref) - Invalid preference provided.");
		}
	}
}
