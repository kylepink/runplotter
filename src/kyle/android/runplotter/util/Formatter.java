package kyle.android.runplotter.util;

/**
 * Formatter provides the formatting and conversion for various functions.
 * 
 * @author Kyle Pink
 */
public class Formatter {
	
	//Prevents initialisation - ANTI-PATTERN.
	private Formatter(){};
	
	//
	//  Time conversion methods.
	//

	/**
	 * Allows for a time in milliseconds to be converted into HHMMSS.
	 * 
	 * @param ms - long value representing the input time.
	 * @param result - int[3] input with: [0] = hh, [1] = mm, [2] = ss. For the
	 *  result to be returned.
	 */
	public static void getHHMMSS(long ms, int[] result) {
		result[0] = (int)(ms / 3600000);
		result[1] = (int)((ms % 3600000) / 60000);
		result[2] = (int)((ms % 60000) / 1000);
	}
	
	/**
	 * Converts a date from long format to a HH:MM:SS string.
	 * 
	 * @param ms - milliseconds to convert to time.
	 * @return - String formatted in HH:MM:SS.
	 */
	public static String getHHMMSS(long ms) {
		return String.format("%d:%02d:%02d", ms / 3600000, (ms % 3600000) / 60000, (ms % 60000) / 1000);
	}
	
	/**
	 * Converts a date from long format to a 'HH hrs MM mins SS secs' string.
	 * 
	 * @param ms - milliseconds to convert to time.
	 * @return - String formatted in 'HH hrs MM mins SS secs'.
	 */
	public static String getAnnotatedHHMMSS(long ms) {
		//calculates hrs, mins, secs.
		int hrs = (int) (ms / 3600000);
		int mins = (int) (ms % 3600000) / 60000;
		int secs = (int) (ms % 60000) / 1000;
		
		//Returns string with only what is required.
		if (hrs > 0){
			return String.format("%d hrs %d mins %d secs", hrs, mins, secs);
		} else if (mins > 0) {
			return String.format("%d mins %d secs", mins, secs);
		} else {
			return String.format("%d secs", secs);
		}
	}
	
	/**
	 * Converts a date from long format to a 'HH hours MM minutes SS seconds' string.
	 * 
	 * @param ms - milliseconds to convert to time.
	 * @return - String formatted in 'HH hrs MM mins SS secs'.
	 */
	public static String getLongAnnotatedHHMMSS(long ms) {
		//calculates hrs, mins, secs.
		int hrs = (int) (ms / 3600000);
		int mins = (int) (ms % 3600000) / 60000;
		int secs = (int) (ms % 60000) / 1000;
		
		//Returns string with only what is required.
		if (hrs > 0){
			return String.format("%d hours %d minutes %d seconds", hrs, mins, secs);
		} else if (mins > 0) {
			return String.format("%d minutes %d seconds", mins, secs);
		} else {
			return String.format("%d seconds", secs);
		}
	}
}
