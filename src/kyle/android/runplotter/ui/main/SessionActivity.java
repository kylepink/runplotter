package kyle.android.runplotter.ui.main;

import java.text.DecimalFormat;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.tracker.SessionTrackerService;
import kyle.android.runplotter.ui.review.SessionDetailsActivity;
import kyle.android.runplotter.util.Distance;
import kyle.android.runplotter.util.Formatter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class SessionActivity extends SherlockActivity {

	private static final int DIALOG_ID_ABANDON_SESSION = 1;
	
	private static final int MINIMUM_UPDATE_FREQUENCY = 7000;

	//UI elements
	private ImageView gpsStatusIcon;
	private TextView runTime;
	private TextView distanceTotal;
	private TextView distanceMetric;
	private TextView speedCurrent;
	private TextView speedMetric;
	private TextView speedAverage;
	private TextView speedAverageMetric;
	private ImageButton stopButton;
	private ImageButton startPauseButton;

	//Service
	private volatile int currentServiceStatus = SessionTrackerService.STATUS_BEFORE;
	private volatile long currentTime = 0;
	private volatile long previousTimeTaken = 0;
	private long minimumUpdateTickCheck = 0;

	//State info / helper
	private int currentGpsStatus = LocationProvider.OUT_OF_SERVICE;
	private TrackerUpdateReciever updateReceiver;
	private TransitionReciever transitionReceiver;
	private LocalBroadcastManager localBroadcaster;

	//preferences.
	private int measurementPref;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_session);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		//Stores views into attributes.
		gpsStatusIcon = (ImageView) findViewById(R.id.session_imageview_gps_status);
		runTime = (TextView) findViewById(R.id.session_text_time);
		distanceTotal = (TextView) findViewById(R.id.session_text_total_distance);
		distanceMetric = (TextView) findViewById(R.id.session_text_distance_metric);
		speedCurrent = (TextView) findViewById(R.id.session_text_current_speed);
		speedMetric = (TextView) findViewById(R.id.session_text_speed_metric);
		speedAverage = (TextView) findViewById(R.id.session_text_average_speed);
		speedAverageMetric = (TextView) findViewById(R.id.session_text_average_speed_metric);
		stopButton = (ImageButton) findViewById(R.id.session_imagebutton_stop);
		startPauseButton = (ImageButton) findViewById(R.id.session_imagebutton_play_pause);


		//Retrieves settings.
		RunPlotterSettings settings =  ((RunPlotterApplication) getApplicationContext()).getSettings();
		measurementPref = settings.getDistanceMeasurementType();


		//Sets static metric UI elements based on settings.
		distanceMetric.setText(Distance.getShortStringPreference(measurementPref));
		speedMetric.setText(Distance.getShortPerHourStringPreference(measurementPref));
		speedAverageMetric.setText(Distance.getShortPerHourStringPreference(measurementPref));

		stopButton.setEnabled(false);


		//Initiates local broadcaster, update receiver, transition receiver.
		localBroadcaster = LocalBroadcastManager.getInstance(this);
		updateReceiver = new TrackerUpdateReciever();
		transitionReceiver = new TransitionReciever();

		transitionReceiver.startReceiving();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		transitionReceiver.stopReceiving();
	}

	@Override
	protected void onResume() {
		super.onResume();

		updateReceiver.startReceiving();
	}

	@Override
	protected void onPause() {
		super.onPause();

		updateReceiver.stopReceiving();
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			showDialog(DIALOG_ID_ABANDON_SESSION);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed(){
		showDialog(DIALOG_ID_ABANDON_SESSION);
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id, Bundle args) {

		Dialog returnDialog;

		switch (id) {
		case DIALOG_ID_ABANDON_SESSION:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Abandon Activity?");
			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setMessage("Are you sure you want to leave this Activity?\n\nData will be lost!");
			builder.setNegativeButton("Yes, leave.", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					transitionReceiver.notifyToKillDiscard();
				}
			});
			builder.setPositiveButton("No, stay.", null);
			returnDialog = builder.create();
			break;

		default:
			returnDialog = super.onCreateDialog(id, args);
			break;
		}

		return returnDialog;
	}

	private void updateUiElements(){
		switch(currentServiceStatus){
		case SessionTrackerService.STATUS_BEFORE:
			stopButton.setEnabled(false);
			startPauseButton.setImageResource(R.drawable.ic_session_action_play);
			break;

		case SessionTrackerService.STATUS_RUNNING:
			stopButton.setEnabled(true);
			startPauseButton.setImageResource(R.drawable.ic_session_action_pause);
			break;

		case SessionTrackerService.STATUS_PAUSED:
			stopButton.setEnabled(true);
			startPauseButton.setImageResource(R.drawable.ic_session_action_play);
			break;

		case SessionTrackerService.STATUS_AFTER:
			stopButton.setEnabled(false);
			startPauseButton.setEnabled(false);
			break;

		default:
			throw new IllegalStateException("SessionActivity - Invalid state provided for Tracking state.");
		}

		//TODO set audio status button.
	}


	//
	//  Click Listeners.
	//

	public void audioToggle(View view) {
		//TODO
	}

	public void stop(View view) {
		stopRun();
	}

	public void startPlayToggle(View view) {
		switch(currentServiceStatus){
		case SessionTrackerService.STATUS_BEFORE:
			startRun();
			break;

		case SessionTrackerService.STATUS_RUNNING:
			pauseRun();
			break;

		case SessionTrackerService.STATUS_PAUSED:
			resumeRun();
			break;

		default:
			throw new IllegalStateException("SessionActivity - Invalid tracking state provided.");
		}
	}


	//
	//  Service manipulation methods.
	//

	private void startRun(){
		sendServiceCommand(SessionTrackerService.COMMAND_START);
	}

	private void pauseRun(){
		sendServiceCommand(SessionTrackerService.COMMAND_PAUSE);
	}

	private void resumeRun(){
		sendServiceCommand(SessionTrackerService.COMMAND_RESUME);
	}

	private void stopRun(){
		sendServiceCommand(SessionTrackerService.COMMAND_STOP);
	}

	private void sendServiceCommand(int commandId) {
		Log.i("SERVICECOMMAND SEND", "OCCURS ID: " + commandId);
		//Creates intent with status, sends command for status.
		Intent intent = new Intent();
		intent.setAction(SessionTrackerService.INTENT_ACTION_COMMAND);
		intent.putExtra(SessionTrackerService.COMMAND_TYPE, commandId);
		localBroadcaster.sendBroadcast(intent);
	}


	//
	//  Components to update the UI
	//

	private Handler timeUpdateHandler = new Handler() {
		private int[] convertedTime = new int[3];

		@Override
		public void handleMessage(Message msg) {
			long time = (Long) msg.obj;

			//Converts time into hours, minutes, seconds units of time - sets time.
			Formatter.getHHMMSS(time, convertedTime);

			runTime.setText((convertedTime[0] > 9?convertedTime[0]:"0" + convertedTime[0]) + ":" +
					(convertedTime[1] < 10?"0":"") + convertedTime[1] + ":" + 
					(convertedTime[2] < 10?"0":"") + convertedTime[2]);
		}
	};

	private Handler uiUpdateHandler = new Handler() {
		private DecimalFormat formatter = new DecimalFormat("0.00");

		@Override
		public void handleMessage(Message msg) {
			//Retrieves data from message.
			Intent intent = (Intent) msg.obj;

			//Updates status and time.
			currentServiceStatus = intent.getIntExtra(SessionTrackerService.UPDATE_DATA_INFO_STATUS, -1);
			currentTime = intent.getLongExtra(SessionTrackerService.UPDATE_DATA_INFO_TIME, 0L);
			previousTimeTaken = SystemClock.elapsedRealtime();

			//Retrieves all other data.
			int gpsStatus = intent.getIntExtra(SessionTrackerService.UPDATE_DATA_INFO_GPS_PROVIDER_STATUS, -1);
			double totalDistanceM = intent.getFloatExtra(SessionTrackerService.UPDATE_DATA_INFO_TOTAL_DIST_M, 0f);
			float currentSpeedMps = intent.getFloatExtra(SessionTrackerService.UPDATE_DATA_INFO_SPEED_MPS, 0f);
			float avgSpeedMps = intent.getFloatExtra(SessionTrackerService.UPDATE_DATA_INFO_SPEED_AVG_MPS, 0f);

			//Updates GPS icon if needed.
			if (currentGpsStatus != gpsStatus){
				currentGpsStatus = gpsStatus;

				switch(currentGpsStatus) {
				case LocationProvider.AVAILABLE:
					gpsStatusIcon.setImageResource(R.drawable.ic_gps_status_on);
					break;

				case LocationProvider.TEMPORARILY_UNAVAILABLE:
					gpsStatusIcon.setImageResource(R.drawable.ic_gps_status_temp_off);
					break;

				default:
				case LocationProvider.OUT_OF_SERVICE:
					gpsStatusIcon.setImageResource(R.drawable.ic_gps_status_off);
					break;


				}
			}

			//Updates distance and speed text in preferred metric.
			distanceTotal.setText(formatter.format(Distance.metresToPreference(measurementPref, totalDistanceM)));
			speedCurrent.setText(formatter.format(Distance.mpsToPreferencePerHour(measurementPref, currentSpeedMps)));
			speedAverage.setText(formatter.format(Distance.mpsToPreferencePerHour(measurementPref, avgSpeedMps)));


			updateUiElements();
		}
	};


	private class TrackerUpdateReciever extends BroadcastReceiver implements Runnable {
		private final Object START_LOCK = new Object(); 
		private volatile boolean running = false;
		private Thread thread;

		@Override
		public void onReceive(Context context, Intent intent) {
			//Stops this updater if intent indicates service is finished.
			if (intent.getIntExtra(SessionTrackerService.UPDATE_DATA_INFO_STATUS, -1) == SessionTrackerService.STATUS_AFTER) {
				transitionReceiver.notifyToKillSave();
				stopReceiving();
			}

			//Sends intent to UI handler.
			Message msg = new Message();
			msg.obj = intent;
			uiUpdateHandler.sendMessage(msg);
		}

		//Used to restart the UI updater provided finish() had been called.
		public void startReceiving(){
			Log.i(this.getClass().getName(), "start of update reciever occurred.");

			//Does not attempt restart, as currently running.
			synchronized(START_LOCK) {
				if(running){
					throw new IllegalStateException("InfoUpdater.start() - Invalid state, attempted to start UI Updater twice.");
				}
				running = true;
			}

			//Registers BC receiver.
			localBroadcaster.registerReceiver(this, new IntentFilter(SessionTrackerService.INTENT_ACTION_UPDATE));

			//Creates new thread to start.
			thread = new Thread(this);
			thread.start();
		}

		public void stopReceiving(){
			Log.i(this.getClass().getName(), "stop of update reciever occurred.");
			//Unregisters receiver and stops updater.
			localBroadcaster.unregisterReceiver(this);
			running = false;
		}

		@Override
		public void run() {
			Message msg;

			//Sends command for update, for initial data.
			sendServiceCommand(SessionTrackerService.COMMAND_SEND_UPDATE);

			//Loops continuously until finish is called from external source.
			while(running){
				//Updates time if state is running.
				if (currentServiceStatus == SessionTrackerService.STATUS_RUNNING) {
					long newTimeTaken = SystemClock.elapsedRealtime();
					currentTime += (newTimeTaken - previousTimeTaken);
					previousTimeTaken = newTimeTaken;

					//Creates Message
					msg = new Message();
					msg.obj = Long.valueOf(currentTime);

					//Passes time to time updater handler.
					timeUpdateHandler.sendMessage(msg);

				} else {
					//Else updates the previous time taken if paused/stopped. 
					previousTimeTaken = SystemClock.elapsedRealtime(); 
				}

				//sleeps briefly between updates.
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				//Requests update every MINIMUM_UPDATE_FREQUENCY.
				long updateTick = SystemClock.elapsedRealtime() / MINIMUM_UPDATE_FREQUENCY;
				if (updateTick > minimumUpdateTickCheck) {
					sendServiceCommand(SessionTrackerService.COMMAND_SEND_UPDATE);
					minimumUpdateTickCheck = updateTick;
				}
			}
		}
	}


	private class TransitionReciever extends BroadcastReceiver {
		private boolean killCommandIssued = false;

		public void notifyToKillSave() {
			if (shouldKillCommandBeSent()) {
				SessionActivity.this.sendServiceCommand(SessionTrackerService.COMMAND_KILL_SAVE);
			}
		}

		public void notifyToKillDiscard() {
			if (shouldKillCommandBeSent()) {
				SessionActivity.this.sendServiceCommand(SessionTrackerService.COMMAND_KILL_DISCARD);
			}
		}

		//Method will only return true once.
		private synchronized boolean shouldKillCommandBeSent() {
			boolean shouldKillCommandBeSent = !killCommandIssued;
			killCommandIssued = true;
			return shouldKillCommandBeSent;
		}


		//Used to start/stop the receiver.
		public void startReceiving(){
			localBroadcaster.registerReceiver(this, new IntentFilter(SessionTrackerService.INTENT_ACTION_TRANSITION));
		}

		public void stopReceiving(){
			localBroadcaster.unregisterReceiver(this);
		}


		@Override
		public void onReceive(Context context, Intent broadcastIntent) {
			Log.i("transitionUpdater", "transition recieved.");

			//Performs transition based on id.
			int transitionId = broadcastIntent.getIntExtra(SessionTrackerService.TRANSITION_TYPE, -1);

			switch(transitionId) {
			case SessionTrackerService.TRANSITION_TYPE_SESSION_FINISH_SAVE:
				long sessionId = broadcastIntent.getLongExtra(SessionTrackerService.TRANSITION_SESSION_SAVE_ID, -1);

				//Passes session id to next class.
				Intent activityIntent = new Intent(SessionActivity.this, SessionDetailsActivity.class);
				activityIntent.putExtra(RunPlotterApplication.INTENT_SESSION_ID, sessionId);
				startActivity(activityIntent);

				SessionActivity.this.finish();
				break;

			case SessionTrackerService.TRANSITION_TYPE_SESSION_FINISH_DISCARD:
				SessionActivity.this.finish();
				break;
			}
		}
	}
}





