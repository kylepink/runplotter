package kyle.android.runplotter.ui.main;

import java.util.List;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.data.model.SessionType;
import kyle.android.runplotter.data.model.TrackingType;
import kyle.android.runplotter.tracker.SessionTrackerService;
import kyle.android.runplotter.ui.review.OverviewGraphsActivity;
import kyle.android.runplotter.ui.review.SessionSelectionActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.ViewFlipper;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

/**
 * Activity to allow for the starting of new a {@link Session} through {@link SessionActivity},
 * also allows for the navigation to {@link SettingsActivity}, {@link ReviewTypeSelectionActivity}, {@link AboutActivity}.
 * 
 * @author Kyle Pink
 */
public class NewSessionSetupActivity extends SherlockActivity {

	//Session selected variables.
	private SessionType sessionTypeSelected = null;
	private TrackingType trackingTypeSelected = null;
	private ViewFlipper trackingTypeOptions = null;

	//Spinner items.
	private ArrayAdapter<SessionType> availableSessionTypesAdapter;
	private ArrayAdapter<TrackingType> availableTrackingTypesAdapter;
	private Spinner sessionTypeSpinner;
	private Spinner trackingTypeSpinner;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_new_session_setup);

		//Retrieves and sets all of the screen elements to local variables.
		sessionTypeSpinner = (Spinner) findViewById(R.id.session_setup_spinner_session_type);
		trackingTypeSpinner = (Spinner) findViewById(R.id.session_setup_spinner_tracking_type);
		trackingTypeOptions = (ViewFlipper) findViewById(R.id.session_setup_flipper_tracking_type_options);

		//Sets the session types for selection.
		DataLoader db = ((RunPlotterApplication) getApplicationContext()).getDataLoader();
		db.open();
		List<SessionType> tempStoredSessionTypes = db.getAllSessionTypes();	
		db.close();
		db = null;

		//Sets adapter for dialog.
		availableSessionTypesAdapter = new ArrayAdapter<SessionType>(this, android.R.layout.simple_spinner_item, tempStoredSessionTypes);
		availableSessionTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		//Sets ListAdapter for the session spinner.
		sessionTypeSpinner.setAdapter(availableSessionTypesAdapter);

		//Sets Listener for spinner.
		sessionTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				sessionTypeSelected = (SessionType) parent.getItemAtPosition(pos);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		//sets up adapter for dialog.
		availableTrackingTypesAdapter = new ArrayAdapter<TrackingType>(this, android.R.layout.simple_spinner_item, TrackingType.values());
		availableTrackingTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		//Sets ListAdapter for the tracking type spinner.
		trackingTypeSpinner.setAdapter(availableTrackingTypesAdapter);
		trackingTypeSpinner.setEnabled(false); //TODO REMOVE

		//Sets Listener for spinner.
		trackingTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				trackingTypeSelected = (TrackingType) parent.getItemAtPosition(pos);
				trackingTypeOptions.setDisplayedChild(pos);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		//Skips straight to SessionActivity if service is running.
		if (SessionTrackerService.isTrackerServiceRunning(this)) {
			Log.i(getClass().getName(), "Skipping to current Activity.");

			//Starts activity.
			Intent intent = new Intent(NewSessionSetupActivity.this, SessionActivity.class);
			startActivity(intent);
		}
	}


	@Override
	public void onPause(){
		super.onPause();
	}

	@Override
	public void onStop(){
		super.onStop();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.main_new_session_setup, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		
		case R.id.session_setup_item_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			return true;
			
		case R.id.session_setup_item_manage_types:
			//TODO
			return true;
			
		case R.id.session_setup_item_manage_labels:
			//TODO
			return true;
			
		case R.id.session_setup_item_review_sessions:
			startActivity(new Intent(this, SessionSelectionActivity.class));
			return true;
			
		case R.id.session_setup_item_review_graphed_reports:
			startActivity(new Intent(this, OverviewGraphsActivity.class));
			return true;
			
		case R.id.session_setup_item_about:
			startActivity(new Intent(this, AboutActivity.class));
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	//
	//  Button listeners
	//
	public void startSession(View view) {
		//Starts service for tracker.
		Intent serviceIntent = new Intent(this, SessionTrackerService.class);
		serviceIntent.putExtra(RunPlotterApplication.INTENT_SESSION_TYPE_ID, sessionTypeSelected.getId());
		serviceIntent.putExtra(RunPlotterApplication.INTENT_SESSION_TRACKING_TYPE, trackingTypeSelected.ordinal());
		startService(serviceIntent);

		//Starts activity.
		Intent intent = new Intent(NewSessionSetupActivity.this, SessionActivity.class);
		startActivity(intent);
	}
}
