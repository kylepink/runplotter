package kyle.android.runplotter.ui.main;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;

/**
 * Preference Activity which displays and allows the user to change Settings for
 * the runplotter application. Changes made by the user are persistent.
 * 
 * @author Kyle Pink
 */
public class SettingsActivity extends SherlockPreferenceActivity {
	
	private CheckBoxPreference inactivityPref;
	private CheckBoxPreference maxTimePref;
	private CheckBoxPreference notifyInactivityPref;
	private CheckBoxPreference saveSessionInactivityPref;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		
		//Sets up settings(prefs)
		((RunPlotterApplication) getApplicationContext()).clearSettings();
		addPreferencesFromResource(R.xml.runplotter_settings);
		
		//Prefs
		inactivityPref = (CheckBoxPreference)findPreference("inactivityChecker");
		maxTimePref = (CheckBoxPreference)findPreference("maxTime");
		notifyInactivityPref = (CheckBoxPreference)findPreference("notifyInactivityMaxTimeActivation");
		saveSessionInactivityPref = (CheckBoxPreference)findPreference("saveSessionOnInactivityMaxTime");
		
		//Provides OR dependency on two different preference items.
		setupInactivityPrefsDependency();
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	if (item.getItemId() == android.R.id.home) {
    		finish();
    		return true;
    	} else {
    		return super.onOptionsItemSelected(item);
    	}
    }
	
	
	//
	//  Preference's special behaviour.
	//
	
	private void setupInactivityPrefsDependency() {
		//Initial state setup.
		boolean initalState = inactivityPref.isChecked() || maxTimePref.isChecked();
		
		notifyInactivityPref.setEnabled(initalState);
		saveSessionInactivityPref.setEnabled(initalState);
		
		//dynamic state management.
		inactivityPref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				
				boolean anEnabledInactivityPref = existsTrue((Boolean)newValue, maxTimePref.isChecked());
				
				notifyInactivityPref.setEnabled(anEnabledInactivityPref);
				saveSessionInactivityPref.setEnabled(anEnabledInactivityPref);
				return true;
			}
		});
		maxTimePref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				
				boolean anEnabledInactivityPref = existsTrue((Boolean)newValue, inactivityPref.isChecked());
				
				notifyInactivityPref.setEnabled(anEnabledInactivityPref);
				saveSessionInactivityPref.setEnabled(anEnabledInactivityPref);
				return true;
			}
		});
	}
	
	//Pointless, but may reuse for other settings..
	private boolean existsTrue(boolean... values) {
		for(boolean value : values) {
			if (value) {
				return true; 
			}
		}
		
		return false;
	}
}
