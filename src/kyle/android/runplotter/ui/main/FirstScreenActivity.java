package kyle.android.runplotter.ui.main;

import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.db.RunPlotterSQLiteHelper;
import kyle.android.runplotter.tracker.SessionTrackerService;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.actionbarsherlock.app.SherlockActivity;

/**
 * {@link FirstScreenActivity} is responsible for displaying display on startup and performing
 * minor operations on a separate thread, these minor operations include initial setup and db migration.
 * 
 * @author Kyle Pink
 */
public class FirstScreenActivity extends SherlockActivity {	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//TODO hack like, would be better way?
		//Check if a current TrackerService exists then navigate from this fast.
		if (SessionTrackerService.isTrackerServiceRunning(this)){
			Log.i(this.getClass().getName(), "Skipping screen.");
			startActivity(new Intent(FirstScreenActivity.this, NewSessionSetupActivity.class));
			finish();
		} else {
			new RunPlotterSplashScreenSetupTask().execute("");
		}
	}


	private void progressFromSplashScreen() {
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run() {
				startActivity(new Intent(FirstScreenActivity.this, NewSessionSetupActivity.class));
				finish();
			}
		}, 250);
	}


	//
	//  Setup task which runs on different thread, updates dialog on UI for user.
	//
	private class RunPlotterSplashScreenSetupTask extends AsyncTask<String, String, String> {

		private ProgressDialog dialog;
		private RunPlotterSettings settings;

		public RunPlotterSplashScreenSetupTask() {
			//builds dialog.
			dialog = new ProgressDialog(FirstScreenActivity.this);
			dialog.setMessage("");
			dialog.setCancelable(false);

			//Creates settings.
			settings = ((RunPlotterApplication) getApplicationContext()).getSettingsCheckDefaults();
		}

		@Override
		protected void onProgressUpdate(String... values) {
			//Shows dialog if needed, then shows message.
			if (!dialog.isShowing()) {
				dialog.show();
			}
			dialog.setMessage(values[0]);
		}

		@Override
		protected String doInBackground(String... params) {
			//Performs first time setup.
			if (settings.isFirstTime()) {
				publishProgress("Performing first time setup...");

				//Db init.
				DataLoader db = ((RunPlotterApplication) getApplicationContext()).getDataLoader();
				db.open();
				db.close();

				//Sets and saves db version, clears not first time.
				settings.setVersionsAndFirstTimeFlag(RunPlotterSQLiteHelper.DATABASE_VERSION);
			}

			//TODO Check for backwards db version, will lose data if continue.
			//DB migration if needed.
			if (settings.getPrevDatabaseVersion() != RunPlotterSQLiteHelper.DATABASE_VERSION) {
				publishProgress("Performing data migration for new version...");

				//Db init.
				DataLoader db = ((RunPlotterApplication) getApplicationContext()).getDataLoader();
				db.open();
				db.close();

				//sets db version
				settings.setVersionsAndFirstTimeFlag(RunPlotterSQLiteHelper.DATABASE_VERSION);
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			//Hides dialog if needed, then progresses.
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			progressFromSplashScreen();
		}
	}
}