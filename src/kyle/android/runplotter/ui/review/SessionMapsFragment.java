package kyle.android.runplotter.ui.review;

import kyle.android.runplotter.data.model.SessionData;
import kyle.android.runplotter.data.model.SessionDataPoint;
import kyle.android.runplotter.tracker.SessionTrackerService;
import kyle.android.runplotter.util.SherlockMapFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

public class SessionMapsFragment extends SherlockMapFragment {

	private GoogleMap map;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View viewRoot = super.onCreateView(inflater, container, savedInstanceState);
		map = getMap();
		return viewRoot;
	}


	//
	//  Public manipulators
	//

	public void setSessionData(SessionData sessionData) {
		map.clear();
		drawSessionData(sessionData);
		setMapView(sessionData);
	}

	private void drawSessionData(SessionData sessionData) {
		if (sessionData == null || sessionData.isEmpty()) {
			return;
		}

		PolylineOptions currentLine = createPolyLineOptions();
		SessionDataPoint previousPoint = null;

		for (SessionDataPoint currentPoint : sessionData) {
			
			//Draws finished line, restarts new line.
			if (previousPoint != null && previousPoint.getStatus() != currentPoint.getStatus()) {
				currentLine.color(getLineColour(previousPoint));
				map.addPolyline(currentLine);

				currentLine = createPolyLineOptions();
			}

			//Adds point in line in path.
			if (!currentPoint.isNull()) {
				currentLine.add(currentPoint.getLatLng());
			}

			previousPoint = currentPoint;
		}

		//Draws final line.
		currentLine.color(getLineColour(previousPoint));
		map.addPolyline(currentLine);
	}
	
	private PolylineOptions createPolyLineOptions() {
		return new PolylineOptions().width(5);
	}

	private void setMapView(final SessionData sessionData) {
		//TODO FIX, currently hack to allow moving of camera using bounds.
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				LatLngBounds bounds = sessionData.getLatLngBounds();

				CameraUpdate cu;
				if (bounds == null) {
					cu = CameraUpdateFactory.newLatLngZoom(new LatLng(0, 0), 0);
				} else {
					cu = CameraUpdateFactory.newLatLngBounds(bounds, 15);
				}
				
				map.moveCamera(cu);
			}
		}, 1500);
	}



	private int getLineColour(SessionDataPoint point) {

		switch(point.getStatus()) {		
		case SessionTrackerService.STATUS_RUNNING:
			return Color.GREEN;

		case SessionTrackerService.STATUS_PAUSED:
			return Color.RED;

		default:
			return Color.MAGENTA;

		}
	}
}
