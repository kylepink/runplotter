package kyle.android.runplotter.ui.review;

import java.util.List;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.db.RunPlotterSQLiteHelper;
import kyle.android.runplotter.data.model.SessionSummaryInfo;
import kyle.android.runplotter.data.model.TrackingType;
import kyle.android.runplotter.ui.review.components.SessionSelectionListAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

/**
 * Simple Activity which displays various sessions which exist, allows for sorting or filtering
 * which can narrow results. User can select actions which navigates them to other activities
 * for reviewing or performing management on other activities.
 * 
 * @author Kyle Pink
 * @see SessionSelectionListAdapter
 */
public class SessionSelectionActivity extends SherlockActivity {

	private boolean renewSessions = false;

	// Sorting spinners
	private Spinner trackingTypeSpinner;
	private Spinner sortingSpinner;

	// List and list data
	private ListView sessionsList;
	private SessionSelectionListAdapter listAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.review_session_selection);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		//Retrieves UI elements
		trackingTypeSpinner = (Spinner) findViewById(R.id.review_session_selection_spinner_session_type);
		sortingSpinner = (Spinner) findViewById(R.id.review_session_selection_spinner_sorting);
		sessionsList = (ListView) findViewById(R.id.review_session_selection_listview_sessions);

		//Creates and sets session adapter.
		listAdapter = new SessionSelectionListAdapter(this);
		sessionsList.setEmptyView(findViewById(R.id.review_session_selection_textview_sessions_empty_display));
		sessionsList.setAdapter(listAdapter);

		//Sets spinner adapters
		Object[] typeData = new Object[]{
				"All Types",
				TrackingType.FREE_TRACKING,
				TrackingType.TIME_TARGET,
				TrackingType.DISTANCE_TARGET
				};
		ArrayAdapter<Object> typeAdapter = new ArrayAdapter<Object>(this, R.layout.simple_centered_spinner_item, typeData);
		typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		trackingTypeSpinner.setAdapter(typeAdapter);

		String[] sortData = new String[]{"Date"};
		ArrayAdapter<Object> sortAdapter = new ArrayAdapter<Object>(this, R.layout.simple_centered_spinner_item, sortData);
		sortAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sortingSpinner.setAdapter(sortAdapter);

		//Sets change listeners for spinners.
		OnItemSelectedListener changeListener = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,int position, long id) {
				populateSessionData();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		};

		trackingTypeSpinner.setOnItemSelectedListener(changeListener);
		sortingSpinner.setOnItemSelectedListener(changeListener);

		//Sets listener for list.
		sessionsList.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				performSessionSelectionAction(((SessionSummaryInfo) listAdapter.getItem(position)).getId());
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		//Populates sessions' data.
		if (renewSessions) {
			populateSessionData();
			renewSessions = false;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	if (item.getItemId() == android.R.id.home) {
    		finish();
    		return true;
    	} else {
    		return super.onOptionsItemSelected(item);
    	}
    }

	//Re-populates session data within list if needed.
	private void populateSessionData() {

		//Retrieves the session's tracking type and sorting selections.
		TrackingType trackingTypeSelection;
		String sortingFieldSelection;

		//Sorting selection
		int sortingSelectionIndex = sortingSpinner.getSelectedItemPosition();
		switch(sortingSelectionIndex) {
		
		case 0:
			sortingFieldSelection = RunPlotterSQLiteHelper.SESSION_COLUMN_DATE;
			break;
			
		default:
			throw new IllegalStateException("SessionSelectionActivity - Invalid sorting selection selected.");
		}

		//Tracking selection.
		Object selectedTrackingType = trackingTypeSpinner.getSelectedItem();
		if (selectedTrackingType.toString().equals("All Types")) {
			trackingTypeSelection = null;
		} else {
			trackingTypeSelection = (TrackingType) selectedTrackingType;
		}

		//Retrieves desired data from database.
		DataLoader db = ((RunPlotterApplication) getApplicationContext()).getDataLoader();
		db.open();
		List<SessionSummaryInfo> data = db.getAllSessionInfo(trackingTypeSelection, sortingFieldSelection);
		db.close();
		db = null;

		//Populates data in adapter and updates session listing.
		listAdapter.setNewData(data);
	}

	private void performSessionSelectionAction(long sessionId){
		//Starts SessionDetailsActivity
		Intent intent = new Intent(this, SessionDetailsActivity.class);
		intent.putExtra(RunPlotterApplication.INTENT_SESSION_ID, sessionId);
		startActivity(intent);
		
		renewSessions = true;
	}
}
