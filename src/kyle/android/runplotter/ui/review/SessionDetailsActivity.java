package kyle.android.runplotter.ui.review;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.DataSaver;
import kyle.android.runplotter.data.model.Label;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.ui.review.components.EditSessionLabelsDialogBuilder;
import kyle.android.runplotter.ui.review.components.OnSessionDataChanged;
import kyle.android.runplotter.util.Distance;
import kyle.android.runplotter.util.Energy;
import kyle.android.runplotter.util.Formatter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

/**
 * Session review activity which allows for the review and display of all overview details
 * to the user. Details are displayed in a list format. Also allows for the user to delete the
 * Session or manage the labels which have been applied.
 * 
 * @author Kyle Pink
 */
public class SessionDetailsActivity extends SherlockActivity {

	//Dialog IDs
	private static final int DIALOG_ID_DELETE_SESSION = 1;
	private static final int DIALOG_ID_EDIT_SESSION_LABELS = 2;

	//Formatting constants
	private static final String TITLE_KEY = "title";
	private static final String CONTENT_KEY = "content";
	private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("EEEE d MMM yyyy");
	private static final DateFormat TIME_FORMATTER = new SimpleDateFormat("h':'mm':'ss a");

	//UI elements
	private SimpleAdapter listAdapter;
	private ListView list;
	private List<Map<String, String>> listItems;
	
	//preferences
	private int distPref;
	private int engPref;
	
	private Session session;
	private RunPlotterSettings settings;
	
	

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.review_session_details);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		//Retrieve data
		long sessionId = getIntent().getExtras().getLong(RunPlotterApplication.INTENT_SESSION_ID);

		//Retrieves session data from db.
		DataLoader db = ((RunPlotterApplication) getApplicationContext()).getDataLoader();
		db.open();
		session = db.getSession(sessionId);
		db.close();
		db = null;
		
		//Settings
		settings = ((RunPlotterApplication) getApplicationContext()).getSettings();
		distPref = settings.getDistanceMeasurementType();
		engPref = settings.getEnergyMeasurementType();
		
		//UI setup
		list = (ListView) this.findViewById(R.id.session_details_list);
		populateSessionData();
	}
	
	
	//
	//  Dialogs
	//

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog newDialog;

		switch(id) {
		case DIALOG_ID_DELETE_SESSION:
			//Creates the delete dialog if none exists.
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Delete Activity?");
			builder.setMessage("Do you want to delete this activity?\n\nThis action will be irreversible.\n\nDelete activity data?");
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface currentDialog, int which) {
					deleteSession();
				}
			});
			builder.setNegativeButton("No", null);
			newDialog = builder.create();
			break;

		case DIALOG_ID_EDIT_SESSION_LABELS:
			EditSessionLabelsDialogBuilder dBuilder = new EditSessionLabelsDialogBuilder(this, session, 
					new OnSessionDataChanged() {
				@Override
				public void onSessionDataChanged() {
					sessionLabelsChanged();
				}
			});
			newDialog = dBuilder.create();
			break;

		default:
			newDialog = super.onCreateDialog(id);
		}

		return newDialog;
	}
	
	
	//
	//  Menus
	//

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.review_session_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
			
		case R.id.session_review_item_edit_labels:
			showDialog(DIALOG_ID_EDIT_SESSION_LABELS);
			return true;
			
		case R.id.session_review_item_delete_session:
			showDialog(DIALOG_ID_DELETE_SESSION);
			return true;
			
		case R.id.session_review_item_graphs:
		case R.id.session_review_item_maps:
			reviewNavigation(item.getItemId());
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}


	private void reviewNavigation(int id) {
		//Data for new activity.
		Intent intent;

		//Creates specific intent with data.
		switch(id) {			
		case R.id.session_review_item_graphs:
			intent = new Intent(this, SessionGraphsActivity.class);
			break;
			
		case R.id.session_review_item_maps:
			intent = new Intent(this, SessionMapsActivity.class);
			break;
			
		default:
			throw new IllegalStateException("BaseSessionReviewActivity - reviewNavigation(" + id + ") - incorrect value provided for navigation.");
		}
		intent.putExtra(RunPlotterApplication.INTENT_SESSION_ID, session.getId());

		//Starts activity
		startActivity(intent);
	}
	
	
	//
	// functions
	//
	
	private void deleteSession(){
		//opens db, deletes session, closes db. 
		DataSaver db = ((RunPlotterApplication) getApplicationContext()).getDataSaver();
		db.open();
		db.deleteSession(session);
		db.close();
		db = null;

		//Notifies deletion of data.
		Toast.makeText(this, "Activity has been deleted.", Toast.LENGTH_LONG).show();

		//Finishes session.
		this.finish();
	}
	
	
	//
	// Populating list data
	//
	
	private void populateSessionData() {
		listAdapter = createListAdapter();
		list.setAdapter(listAdapter);
	}

	private SimpleAdapter createListAdapter() {
		//Date formatting prep
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(session.getDate());
		Date date = cal.getTime();

		//Populates items in the listing.
		listItems = new ArrayList<Map<String, String>>();
		listItems.add(createItem("Date", DATE_FORMATTER.format(date)));
		listItems.add(createItem("Time", TIME_FORMATTER.format(date)));
		listItems.add(createItem("Type", session.getSessionType().toString()));
		listItems.add(createItem("Mode", session.getTrackingType().toString()));
		listItems.add(createItem("Distance", String.format("%.3f ", Distance.metresToPreference(distPref, session.getDistance())) +
				Distance.getLongStringPreference(distPref)));
		listItems.add(createItem("Duration", Formatter.getLongAnnotatedHHMMSS(session.getDuration())));
		listItems.add(createItem("Paused Duration", Formatter.getLongAnnotatedHHMMSS(session.getPauseDuration())));
		listItems.add(createItem("Steps", String.valueOf(session.getSteps())));
		listItems.add(createItem(Energy.getLongCapitalisedStringPreference(engPref),
				String.valueOf(Energy.caloriesToPreference(engPref, session.getEnergy()))));
		listItems.add(createItem("Labels", createLabelsString()));
		
		return new SimpleAdapter(this, listItems,
				R.layout.simple_list_item_2_modified,
				new String[] {TITLE_KEY, CONTENT_KEY},
				new int[] {android.R.id.text1,
				android.R.id.text2});
	}

	private Map<String, String> createItem(String title, String content) {
		Map<String, String> map = new HashMap<String, String>(2);
		map.put(TITLE_KEY, title);
		map.put(CONTENT_KEY, content);
		return map;
	}
	
	private String createLabelsString() {

		StringBuilder labelsString = new StringBuilder();
		for (Label label : session.getLabels()) {
			labelsString.append(label.getLabel());
			labelsString.append('\n');
		}
		
		if (labelsString.length() != 0)
			return labelsString.substring(0, labelsString.length() - 1);
		else
			return labelsString.toString();
	}

	public void sessionLabelsChanged() {
		listItems.remove(listItems.size() - 1);
		listItems.add(createItem("Labels", createLabelsString()));
		listAdapter.notifyDataSetChanged();
	}
	
	public void sessionDataChanged() {
		populateSessionData();
	}
}
