package kyle.android.runplotter.ui.review;

import java.util.Collection;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.db.RunPlotterSQLiteHelper;
import kyle.android.runplotter.data.model.SessionSummaryInfo;
import kyle.android.runplotter.graphs.GraphDefinition;
import kyle.android.runplotter.graphs.GraphFormatHelper;
import kyle.android.runplotter.graphs.OverviewSessionSpeedsOverTime;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.actionbarsherlock.view.MenuItem;

public class OverviewGraphsActivity extends BaseGraphActivity {

	//Graphs
	private GraphDefinition currentGraphDefinition;
	private int currentGraphDefinitionSelected = -1;

	//Data
	private Collection<SessionSummaryInfo> sessions;
	private GraphFormatHelper formatter;
	private RunPlotterSettings settings;
	private Spinner graphSelectionSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		//Retrieves data.
		formatter = new GraphFormatHelper(this);
		settings = ((RunPlotterApplication) getApplication()).getSettings();
		DataLoader db = ((RunPlotterApplication) getApplication()).getDataLoader();
		db.open();
		sessions = db.getAllSessionInfo(null, RunPlotterSQLiteHelper.SESSION_COLUMN_DATE);
		db.close();
		db = null;
		
		//Sets UI
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.simple_centered_spinner_item,
				getResources().getStringArray(R.array.overview_graphs_selection));
		spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		graphSelectionSpinner = (Spinner) findViewById(R.id.runplotter_spinner_graph_selection);
		graphSelectionSpinner.setAdapter(spinnerAdapter);
		graphSelectionSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				boolean graphPicked = setGraphDefinition(position);
				if (graphPicked) {
					setGraphDisplay();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				//nothing
			}
		});
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	
	//
	//  Graph related methods
	//

	private boolean setGraphDefinition(int selection) {
		boolean graphChanged = false;

		if (currentGraphDefinitionSelected != selection) {
			switch (selection) {
			case 0:
				currentGraphDefinition = new OverviewSessionSpeedsOverTime(sessions, formatter, settings);
				break;

			default:
				throw new IllegalStateException("OverviewGraphsActivity - Invalid graph item selected.");

			}
			graphChanged = true;
		}

		return graphChanged;
	}


	//
	//  Base Graph Definition
	//

	@Override
	protected int getLayoutResId() {return R.layout.review_graphs;}

	@Override
	protected GraphDefinition getCurrentGraphDefinition() {return currentGraphDefinition;}

}
