package kyle.android.runplotter.ui.review;

import kyle.android.runplotter.R;
import kyle.android.runplotter.graphs.GraphDefinition;

import org.achartengine.GraphicalView;
import org.achartengine.chart.AbstractChart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

/**
 * Simple base activity for graphs which
 * 
 * @author Kyle Pink
 */
public abstract class BaseGraphActivity extends SherlockActivity {

	protected LinearLayout graphLayoutContainer;

	protected GraphicalView graphView;
	protected TextView graphNotes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResId());
		
		LayoutInflater inflater = LayoutInflater.from(this);

		graphLayoutContainer = (LinearLayout) findViewById(R.id.runplotter_linearlayout_graph_holder);
		graphNotes = (TextView) inflater.inflate(R.layout.review_graph_notes, null);
	}

	/**
	 * Returns and resource id of a R.layout.? resource which contains
	 * an LinearLayout item id of R.id.runplotter_linearlayout_graph_holder. The linear layout
	 * should also contain a single item for graph selection (typically a spinner).
	 * 
	 * @return resource id of a layout which contains the desired display with the required id.
	 */
	protected abstract int getLayoutResId();

	/**
	 * Returns the current graph definition which should be used to display charts, graphs and associated data.
	 * Returned value could be null in some situations, be sure to be check. //TODO check true? 
	 * 
	 * @return GraphDefintion which represents the graph/chart currently selected.
	 */
	protected abstract GraphDefinition getCurrentGraphDefinition();

	/**
	 * Provides the chart/graph which is to be displayed within the current view.
	 *  
	 * @return Chart with defines the graph to display.
	 */
	protected AbstractChart getGraphChart() {
		return getCurrentGraphDefinition().getGraphChart();
	}

	/**
	 * Provides the Notes which are relevant to the current corresponding chart.
	 *  
	 * @return string value which represents important notes to be read along with the graph.
	 */
	protected String getGraphNote() {
		GraphDefinition currentGraph = getCurrentGraphDefinition();
		if (currentGraph == null) {
			return null;
		} else {
			return currentGraph.getGraphNote();
		}
	}

	/**
	 * Retrieves required data from the abstract methods {@link BaseGraphActivity#getGraphChart()} and {@link BaseGraphActivity#getGraphNote()}
	 * and displays content on screen. Should only be called when content from both methods will be different 
	 * from last call.
	 */
	protected void setGraphDisplay() {
		//Removes graph and text from views
		int removeCount = graphLayoutContainer.getChildCount() - 1;
		if (removeCount > 1)
			graphLayoutContainer.removeViewsInLayout(1, removeCount);

		//Retrieves chart, places in view.
		LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
		AbstractChart chart = getGraphChart();
		graphView = new GraphicalView(this, chart);
		graphView.setLayoutParams(param);
		graphLayoutContainer.addView(graphView);
		
		//Retrieves notes, places in view if needed.
		String graphNoteText = getGraphNote();
		if (graphNoteText != null) {
			graphNotes.setText(graphNoteText);
			graphLayoutContainer.addView(graphNotes);
		}
	}
}
