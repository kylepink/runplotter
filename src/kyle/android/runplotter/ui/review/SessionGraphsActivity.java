package kyle.android.runplotter.ui.review;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.model.Session;
import kyle.android.runplotter.graphs.GraphDefinition;
import kyle.android.runplotter.graphs.GraphFormatHelper;
import kyle.android.runplotter.graphs.SessionElevationOverTime;
import kyle.android.runplotter.graphs.SessionRunningPausedOverTime;
import kyle.android.runplotter.graphs.SessionSpeedOverTime;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.actionbarsherlock.view.MenuItem;

/**
 * Session review activity which allows for the review and display of session details with graph
 * formats to the user. Allows different formats for displaying different aspects of the session.
 * 
 * @author Kyle Pink
 */
public class SessionGraphsActivity extends BaseGraphActivity {

	private Spinner graphTypeSpinner;

	//Graph selection
	private int prevGraphDefinitionSelection = -1;
	private GraphDefinition currentGraphDefinition;
	
	//Data for graph definitions
	private Session session;
	private GraphFormatHelper formatter;
	private RunPlotterSettings settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);

		//Retrieves session then settings
		long sessionId = getIntent().getExtras().getLong(RunPlotterApplication.INTENT_SESSION_ID);

		DataLoader db = ((RunPlotterApplication) getApplicationContext()).getDataLoader();
		db.open();
		session = db.getSession(sessionId);
		db.close();
		db = null;
		
		formatter = new GraphFormatHelper(this);
		settings = ((RunPlotterApplication) getApplicationContext()).getSettings();
		

		//Adapter for graphs spinner.
		ArrayAdapter<String> selectionAdapter = new ArrayAdapter<String>(this,
				R.layout.simple_centered_spinner_item,
				getResources().getStringArray(R.array.session_review_graphs_selection));
		selectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		//Data and listener for spinner.
		graphTypeSpinner = (Spinner)findViewById(R.id.runplotter_spinner_graph_selection);
		graphTypeSpinner.setAdapter(selectionAdapter);
		graphTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				//Sets graph definition and display if changed.
				boolean graphChanged = setGraphDefinition();
				if (graphChanged) {
					setGraphDisplay();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {	
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		
		case android.R.id.home:
			finish();
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	
	//
	//  Graphs Related method helpers.
	//
	
	private boolean setGraphDefinition() {
		int itemSelected = graphTypeSpinner.getSelectedItemPosition();
		boolean graphChanged = false;

		//Sets graph definition if same and not set previously.
		if (prevGraphDefinitionSelection != itemSelected) {
			prevGraphDefinitionSelection = itemSelected;
			
			//Sets appropriate graph definition.
			switch(itemSelected) {
			
			case 0:
				currentGraphDefinition = new SessionSpeedOverTime(session, formatter, settings);
				break;
				
			case 1:
				currentGraphDefinition = new SessionElevationOverTime(this);
				break;
				
			case 2:
				currentGraphDefinition = new SessionRunningPausedOverTime(this);
				break;
				
			default:
				throw new IllegalStateException("SessionGraphsActivity - Invalid graph item selected.");
			}
			graphChanged = true;
		}
		
		return graphChanged;
	}
	
	
	//
	//  Retrieves display components.
	//

	@Override
	public int getLayoutResId() {return R.layout.review_graphs;}
	
	@Override
	protected GraphDefinition getCurrentGraphDefinition() {return currentGraphDefinition;}
}
