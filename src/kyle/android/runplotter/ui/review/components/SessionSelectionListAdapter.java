package kyle.android.runplotter.ui.review.components;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.RunPlotterSettings;
import kyle.android.runplotter.data.model.SessionSummaryInfo;
import kyle.android.runplotter.util.Distance;
import kyle.android.runplotter.util.Formatter;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * SessionSelectionListAdapter provides a view adapter for {@link SessionSummaryInfo} items,
 * provides a means for the user to get overviews of Sessions. Provides the following information
 * in the following format:
 * 
 * icon - date, startdate
 * icon - distance, duration
 * 
 * Icon displays the TrackingType.
 * 
 * @author Kyle Pink
 */
public class SessionSelectionListAdapter extends BaseAdapter {

	//Format helpers
	private static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("EEE d MMM yyyy',    ' h':'mm':'ss a");
	private final String TIME_FORMAT;

	//Drawables
	private final Drawable iconFree;
	private final Drawable iconTime;
	private final Drawable iconDistance;

	//Instance display helpers
	private int distanceMeasurementPreference;
	private String distanceMeasurementShortString;

	private LayoutInflater inflater;
	private ArrayList<SessionSummaryInfo> items = new ArrayList<SessionSummaryInfo>();

	public SessionSelectionListAdapter(Context context) {
		inflater = LayoutInflater.from(context);

		iconFree = context.getResources().getDrawable(R.drawable.ic_tracking_type_free);
		iconTime = context.getResources().getDrawable(R.drawable.ic_tracking_type_time);
		iconDistance   = context.getResources().getDrawable(R.drawable.ic_tracking_type_distance);

		RunPlotterSettings settings = ((RunPlotterApplication) context.getApplicationContext()).getSettings();
		distanceMeasurementPreference = settings.getDistanceMeasurementType();
		distanceMeasurementShortString = Distance.getShortStringPreference(distanceMeasurementPreference);
		
		TIME_FORMAT = "%.2f " + distanceMeasurementShortString + ",    ";
	}

	public void setNewData(Collection<SessionSummaryInfo> data) {
		this.items.clear();
		this.items.addAll(data);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		Holder holder;
		if (v == null) {
			//Inflates view.
			v = inflater.inflate(R.layout.review_session_selection_list_item, null);

			//Sets holder references.
			holder = new Holder();
			holder.icon = (ImageView) v.findViewById(R.id.review_session_selection_list_item_icon);
			holder.dateTime= (TextView) v.findViewById(R.id.review_session_selection_list_item_date_time);
			holder.distanceDuration = (TextView) v.findViewById(R.id.review_session_selection_list_item_distance_duration);

			v.setTag(holder);

		} else {
			holder = (Holder) v.getTag();
		}

		//Sets data for specific session.
		SessionSummaryInfo session = items.get(position);

		//Sets icon image.
		switch(session.getTrackingType()) {
		case FREE_TRACKING:
			holder.icon.setImageDrawable(iconFree);
			break;
			
		case TIME_TARGET:
			holder.icon.setImageDrawable(iconTime);
			break;
			
		case DISTANCE_TARGET:
			holder.icon.setImageDrawable(iconDistance);
			break;
			
		default:
			throw new IllegalStateException("SessionSelectionListActivity.getView() - Invalid state provided.");
		}

		//Sets date, distance
		holder.dateTime.setText(DATE_TIME_FORMAT.format(session.getDate()));
		holder.distanceDuration.setText(String.format(TIME_FORMAT, Distance.metresToPreference(distanceMeasurementPreference, session.getDistance())) + Formatter.getAnnotatedHHMMSS(session.getDuration()));

		return v;
	}

	//Holds references to items, prevents repeated findViewById calls.
	private static class Holder {
		public ImageView icon;
		public TextView dateTime;
		public TextView distanceDuration;
	}
}
