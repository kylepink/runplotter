package kyle.android.runplotter.ui.review.components;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.DataSaver;
import kyle.android.runplotter.data.model.Label;
import kyle.android.runplotter.data.model.Session;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.widget.Toast;

public class EditSessionLabelsDialogBuilder extends AlertDialog.Builder {

	private Session session;

	//Label collections to indicate changes to be made.
	private HashSet<Label> labelsToApply = new HashSet<Label>();
	private HashSet<Label> labelsToRemove = new HashSet<Label>();

	//Items to make label selection.
	private HashMap<String, Label> availableLabelsMap = new HashMap<String, Label>();
	private String[] availableLabelsStrings;

	private Context context;

	//Listener
	private OnSessionDataChanged labelChangedListener;

	public EditSessionLabelsDialogBuilder(Context context, Session session, OnSessionDataChanged labelChangedListener) {
		super(context);

		this.context = context;
		this.session = session;
		this.labelChangedListener = labelChangedListener;

		//Retrieves all the available Labels from the database.
		DataLoader db = ((RunPlotterApplication) context.getApplicationContext()).getDataLoader();
		db.open();
		Collection<Label> availableLabels = db.getAllLabels();
		db.close();
		db = null;

		//Places applied labels into map/string.
		availableLabelsStrings = new String[availableLabels.size()];
		boolean[] checkedLabels = new boolean[availableLabels.size()];
		int i = 0;
		for(Label label : availableLabels){
			availableLabelsStrings[i] = label.toString();
			availableLabelsMap.put(availableLabelsStrings[i], label);
			checkedLabels[i] = session.contains(label);
			++i;
		}

		//Setting up of the Dialog to add/remove Labels from applied Labels.
		//Builds the dialog object to display SessionType selections. 
		setTitle("Select desired labels:");
		setIcon(android.R.drawable.ic_menu_more);
		setMultiChoiceItems(availableLabelsStrings, checkedLabels, new Dialog.OnMultiChoiceClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				Label itemActivated = availableLabelsMap.get(availableLabelsStrings[which]);
				//labelsToRemove
				//labelsToApply
				if (EditSessionLabelsDialogBuilder.this.session.contains(itemActivated)){
					if (isChecked){
						labelsToRemove.remove(itemActivated);
					} else{
						labelsToRemove.add(itemActivated);
					}
				} else {
					if (isChecked){
						labelsToApply.add(itemActivated);
					} else{
						labelsToApply.remove(itemActivated);
					}
				}
			}
		});
		setNegativeButton("Manage Labels", new OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//TODO manage button for labels?
				Toast.makeText(EditSessionLabelsDialogBuilder.this.context, "NOT IMPLEMENTED", Toast.LENGTH_LONG).show();
				setCurrentAppliedLabels(); //TODO MOVE?
			}
		});
		setPositiveButton("Close", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				setCurrentAppliedLabels();
			}
		});
		setOnCancelListener(new OnCancelListener(){
			@Override
			public void onCancel(DialogInterface arg0) {
				setCurrentAppliedLabels();
			}
		});
	}



	//
	//  Label management related methods.
	//
	
	private void setCurrentAppliedLabels(){
		boolean labelsChanged = false;
		if (labelsToApply.size() != 0){
			addLabelsToSession();
			labelsChanged = true;
		}

		if (labelsToRemove.size() != 0){
			removeLabelsFromSession();
			labelsChanged = true;
		}

		if (labelsChanged){
			//labelsChanged();
			labelChangedListener.onSessionDataChanged();
		}
	}



	private void addLabelsToSession(){
		//Opens db.
		DataSaver db = ((RunPlotterApplication) context.getApplicationContext()).getDataSaver();
		db.open();

		//Iterates through collection and adds labels to the current session.
		for (Label i: labelsToApply){
			db.addLabelToSession(session, i);
		}

		//Closes db.
		db.close();
		db = null;

		//Clears remove data set.
		labelsToApply.clear();
	}

	private void removeLabelsFromSession(){
		//Opens db.
		DataSaver db = ((RunPlotterApplication) context.getApplicationContext()).getDataSaver();
		db.open();

		//Iterates through collection and removes labels from the current session.
		for (Label i: labelsToRemove){
			db.removeLabelFromSession(session, i);
		}

		//Closes db.
		db.close();
		db = null;

		//Clears remove data set.
		labelsToRemove.clear();
	}

}
