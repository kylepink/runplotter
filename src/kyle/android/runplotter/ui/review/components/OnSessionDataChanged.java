package kyle.android.runplotter.ui.review.components;

public interface OnSessionDataChanged {
	public void onSessionDataChanged();
}
