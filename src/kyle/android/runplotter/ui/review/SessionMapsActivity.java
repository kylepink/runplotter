package kyle.android.runplotter.ui.review;

import kyle.android.runplotter.R;
import kyle.android.runplotter.RunPlotterApplication;
import kyle.android.runplotter.data.DataLoader;
import kyle.android.runplotter.data.model.SessionData;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class SessionMapsActivity extends SherlockFragmentActivity {
	
	private SessionMapsFragment mapFrag;
	
	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		setContentView(R.layout.review_session_maps);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		
		//Retrieves fragments
		mapFrag = (SessionMapsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_review_session);

		//Retrieves session data
		long sessionId = getIntent().getExtras().getLong(RunPlotterApplication.INTENT_SESSION_ID);
		
		DataLoader db = ((RunPlotterApplication) getApplication()).getDataLoader();
		db.open();
		SessionData sessionData = db.getSession(sessionId).getSessionData();
		db.close();
		db = null;
		
		mapFrag.setSessionData(sessionData);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
